﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Novo.Models.Entity
{
    [Table("Notification")]
    public  class Notification : Base
    {
       
        public string Message { get; set; }
        public string NotificationType { get; set; }
        public Guid Guid { get; set; }
        public bool? IsDeleted { get; set; }
    }
}
