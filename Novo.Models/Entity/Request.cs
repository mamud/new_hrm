﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Novo.Models.Entity
{
    public class Request:Base
    {
        [Display(Name = "Request")]
        public string RequestName { get; set; }
        [Display(Name = "Request Description")]
        public string RequestDescription { get; set; }
        [ForeignKey("ApplicationUser")]
        [Display(Name = "Assign To")]
        public int AssignToId { get; set; }
        public string Comment { get; set; }
        public int EmployeeId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
        [Display(Name = "Unit Head Approved?")]
        public bool isUnitHeadApproved { get; set; }
        //[Required]
        [Display(Name = "HR Approved?")]
        public bool isHrApproved { get; set; }

        [Display(Name = "Admin Approved?")]
        public bool isAdmApproved { get; set; }

        [Display(Name = "Unit Head Disapproved?")]
        public bool isUnitHeadDisapprove { get; set; }
        //[Required]
        [Display(Name = "HR Disapproved?")]
        public bool isHrDisapprove { get; set; }
    }
}
