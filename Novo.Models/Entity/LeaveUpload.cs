﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Novo.Models.Entity
{
    public class LeaveUpload
    {
        public int Id { get; set; }
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string StaffNumber { get; set; }
        public string Location { get; set; }
        public string NumberOfOutstandingLeaves { get; set; }
        public string CurrentLeaveDays { get; set; }
        public string TotalLeaves { get; set; }
        public string RemainingLeaves { get; set; }
        public string UtilizedLeaves { get; set; }


    }
}
