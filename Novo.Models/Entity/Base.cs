﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Novo.Models.Entity
{
    public class Base
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [DisplayName("Added By")]
        public string AddedBy { get; set; } = DateTime.Now.ToString();
        [DisplayName("Added By")]
        public string UpdatedBy { get; set; } = DateTime.Now.ToString();
        [DisplayName("Added On")]
        public DateTime CreateDate { get; set; } = DateTime.Now;
        [DisplayName("Updated On")]
        public DateTime UpdateDate { get; set; } = DateTime.Now;
        [DisplayName("Is Deleted?")]
        public bool IsDelete { get; set; }
    }
}
