﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Novo.Models.Entity
{
   public class Query : Base
    {
       
        [Display(Name = "Employee")]
        public int EmployeeId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Query Description")]
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
       
        [Display(Name = "Note")]
        [DataType(DataType.MultilineText)]
        public string Note { get; set; }
       
        [Display(Name = "Reason")]
        [DataType(DataType.MultilineText)]
        public string Reason { get; set; }

      
        [Display(Name = "Employee Response")]
        [DataType(DataType.MultilineText)]
        public string EmployeeResponse { get; set; }

       
        [Display(Name = "Unit Head Response")]
        [DataType(DataType.MultilineText)]
        public string UnitHeadResponse { get; set; }

      
        [Display(Name = "HR Response")]
        [DataType(DataType.MultilineText)]
        public string HrResponse { get; set; }

        public Status Status { get; set; }
    }
}
