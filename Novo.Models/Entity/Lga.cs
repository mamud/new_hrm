﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Novo.Models.Entity
{
    [Table("Lga")]
    public class Lga : Base
    { 
            public string Name { get; set; }
            public State State { get; set; }
            
    }
}
