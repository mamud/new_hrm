﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Novo.Models.Entity
{

    public class EmployeeContact : Base
    {
       
        [Display(Name = "Employee")]
        public int EmployeeId { get; set; }

        
        [Display(Name = "Contact Address")]
        public string Address { get; set; }

       
        [Display(Name = "City")]
        public string City { get; set; }

        
        [Display(Name = "State")]
        [ForeignKey("StateId")]
        public int StateId { get; set; }
        public State State { get; set; }

        [Display(Name = "State Of Origin")]
        public string StateOfOrigin { get; set; }

        [Display(Name = "Local Govt Area")]
        [ForeignKey("LGAId")]
        public int LgaId { get; set; }
        public Lga Lga { get; set; }

        [Display(Name = "Region")]
        public int RegionId { get; set; }
        [ForeignKey("RegionId")]
        public Regions Regions { get; set; }

        
        [Display(Name = "Nationality")]
        public string CountryOfOrigin { get; set; }

        [Display(Name = "Beneficiary's Name")]
        public string BeneficiaryName { get; set; }
        [Display(Name = "Phone Number")]
        public string BeneficiaryNo { get; set; }

        [Display(Name = "Contact Address")]
        public string BeneficiaryAddress { get; set; }

        [Display(Name = "Emergency Name")]
        public string EmergencyName { get; set; }
        [Display(Name = "Phone Number")]
        public string EmergencyNo { get; set; }

        [Display(Name = "Contact Address")]
        public string EmergencyAddress { get; set; }

        [Display(Name = "Local Govt Area")]
        public string Lgas { get; set; }

        [Display(Name = "State  Of Origin")]
        public string States { get; set; }

        [Display(Name = "Region")]
        public string Region { get; set; }

    }
}
