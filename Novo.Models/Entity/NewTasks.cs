﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Novo.Models.Entity
{
    public class NewTasks:Base
    {
        [Display(Name = "Task")]
        public string TaskName { get; set; }
        [Display(Name = "Task Type")]
        public string TaskType { get; set; }
        [Display(Name = "Task Description")]
        public string TaskDescription { get; set; }
        [Display(Name = "Assign By")]
        public string AssignBy { get; set; }

        public int EmployeeId { get; set; }

        [Display(Name = "Expected Start")]
        [DataType(DataType.Date)]
        public string ExpectedStartDate { get; set; }
        [Display(Name = "Expected End")]
        [DataType(DataType.Date)]
        public string ExpectedEndDate { get; set; }
        [Display(Name = "Start On")]
        [DataType(DataType.Date)]
        public DateTime StartDate { get; set; }
        [Display(Name = "End On")]
        [DataType(DataType.Date)]
        public DateTime EndDate { get; set; }

        public string Comment { get; set; }

        public string Remark { get; set; }

        public string Status { get; set; }

        [Display(Name = "Unit Head Approved?")]
        public bool isUnitHeadApproved { get; set; }
        //[Required]
        [Display(Name = "HR Approved?")]
        public bool isHrApproved { get; set; }

        [Display(Name = "EMp Approved?")]
        public bool isEmpApproved { get; set; }

        [Display(Name = "Unit Head Disapproved?")]
        public bool isUnitHeadDisapprove { get; set; }
        //[Required]
        [Display(Name = "HR Disapproved?")]
        public bool isHrDisapprove { get; set; }

        [Display(Name = "Assign To")]
        [ForeignKey("ApplicationUser")]
        public int AssignToId { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}
