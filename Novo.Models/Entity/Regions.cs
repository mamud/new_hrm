﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Novo.Models.Entity
{
    [Table("Region")]
    public class Regions : Base
    {
     
        public string Name { get; set; }
        public string Code { get; set; }
        public Country Country { get; set; }
       
    }
}
