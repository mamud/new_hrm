﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Novo.Models.Entity
{
    public class ApplicationUser : IdentityUser<int>
    {



        [Required]
        [Display(Name = "Employee Firstname")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Employee Lastname")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "Employee Unit")]
        public int UnitsId { get; set; }
        [ForeignKey("UnitsId")]
        public Units Units { get; set; }



        


        public virtual ICollection<UserToken> UserTokens { get; set; }

        public virtual ICollection<UserRole> Roles { get; set; }

        public virtual ICollection<UserLogin> Logins { get; set; }

        public virtual ICollection<UserClaim> Claims { get; set; }

    }
}
