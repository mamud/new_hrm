﻿using Microsoft.AspNetCore.Http;
using Novo.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Novo.Models.ViewModels
{
    public class EmployeeEditVm : Base
    {
        [Required]
        [Display(Name = "Employee Id")]
        public int EmployeeId { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        public string Fullname { get; set; }

        [Required]
        [Display(Name = "Blood Group")]
        public string BloodGroup { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Date Of Birth")]
        public string DOB { get; set; }

        [Required]
        [Display(Name = "Gender")]
        public string Gender { get; set; }

        [Required]
        [Display(Name = "Marital Status")]
        public string MaritalStatus { get; set; }


        [Display(Name = "Employee Designation")]
        public string Designation { get; set; }


        [Display(Name = "Employee Code / No")]
        public string EmployeeNo { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string Email { get; set; }


        [Display(Name = "Mode Of Employment")]
        public string ModeOfEmployment { get; set; }

        //[Required]
        //[Display(Name = "Employee Unit")]
        //public int UnitsId { get; set; }
        //[ForeignKey("UnitsId")]
        //public Units Units { get; set; }



        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        [Display(Name = "Entry Level")]
        public string EntryLevel { get; set; }


        [Display(Name = "Unit Head?")]
        public bool IsUnitHead { get; set; }


        [Display(Name = "Dept. Head?")]
        public bool IsDeptHead { get; set; }


        [Display(Name = "Regional Head?")]
        public bool IsRegionalHead { get; set; }


        [Display(Name = "Current Level")]
        public string CurrentLevel { get; set; }


        [Display(Name = "Last Promotion")]
        [DataType(DataType.Date)]
        public string LastPromotion { get; set; }


        [DataType(DataType.Date), Display(Name = "Date Employed")]
        public DateTime DateOfEmployment { get; set; }


        [DataType(DataType.Date), Display(Name = "Left On")]
        public DateTime DateOfLeaving { get; set; }


        [Display(Name = "Years Of Experience")]
        public string YearsOfExperience { get; set; }

        [Display(Name = "Image")]
        public IFormFile Image { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string CUG { get; set; }
    }
}
