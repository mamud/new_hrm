﻿using Novo.Models.Entity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Novo.Models.ViewModels
{
    public class EmployeeViewModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Employee")]
        public int EmployeeId { get; set; }

        [Required]
        [Display(Name = "Full Name")]
        public string Fullname { get; set; }

        [Required]
        [Display(Name = "Contact Address")]
        public string Address { get; set; }

        [Required]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required]
        [Display(Name = "State")]
        public int StateId { get; set; }
        public State State { get; set; }
       
        [Required]
        [Display(Name = "Blood Group")]
        public string BloodGroup { get; set; }

        [Required]
        [Display(Name = "Employee Designation")]
        public string Designation { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public string EmploymentStatus { get; set; }

        

     

        [Required]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email Address")]
        public string Email { get; set; }


        [Required]
        [Display(Name = "Employee Code / No")]
        public string EmployeeNo { get; set; }

        [Required]
        [Display(Name = "Mode Of Employment")]
        public string ModeOfEmployment { get; set; }

        [Display(Name = "Leave HandOver Note")]
        [DataType(DataType.MultilineText)]
        public string HandOver { get; set; }

        public string Image { get; set; }

        [Display(Name = "CUG")]
        [DataType(DataType.PhoneNumber)]
        public string Cug { get; set; }


        [Required]
        [Display(Name = "Job Title")]
        public string JobTitle { get; set; }

        //public string Name { get; set; }

        //public string Description { get; set; }

        [Required]
        [Display(Name = "Entry Level")]
        public string EntryLevel { get; set; }

        [Required]
        [Display(Name = "Entry Level")]
        public string CurrentLevel { get; set; }

        [Required]
        [Display(Name = "Last Promotion")]
        [DataType(DataType.Date)]
        public string LastPromotion { get; set; }

        [Required]
        [DataType(DataType.Date), Display(Name = "Date Employed")]
        public DateTime DateOfEmployment { get; set; }

        [Required]
        [DataType(DataType.Date), Display(Name = "Left On")]
        public DateTime DateOfLeaving { get; set; }

        [Required]
        [DataType(DataType.Date), Display(Name = "Years Of Experience")]
        public string YearsOfExperience { get; set; }

        [DisplayName("Added By")]
        public string AddedBy { get; set; }
        [DisplayName("Added By")]
        public string UpdatedBy { get; set; }
        [DisplayName("Added On")]
        public DateTime CreateDate { get; set; }
        [DisplayName("Updated On")]
        public DateTime UpdateDate { get; set; }
        [DisplayName("Is Deleted?")]
        public bool IsDelete { get; set; }

        public string UnitName { get; set; }

        public string DeptName { get; set; }

        public string StateName { get; set; }

        [Required]
        [Display(Name = "Unit Head")]
        public ApplicationUser UnitHead { get; set; }

        [Required]
        [Display(Name = "State of Origin")]
        public State StateOfOrigin { get; set; }

        [Required]
        [Display(Name = "Local Govt Area")]
        public Lga LgaOrigin { get; set; }

        [Display(Name = "Region")]
        public int RegionId { get; set; }
        [ForeignKey("RegionId")]
        public Regions Regions { get; set; }

        [Required]
        [Display(Name = "Employee Unit")]
        public int UnitsId { get; set; }
        [ForeignKey("UnitsId")]
        public Units Units { get; set; }


        [Required]
        [Display(Name = "Nationality")]
        public string CountryOfOrigin { get; set; }

        [Required]
        [Display(Name = "Academic Qualification")]
        public string Academic { get; set; }
        [Required]
        [Display(Name = "Academic Qualification Description")]
        [DataType(DataType.MultilineText)]
        public string AcademicDescription { get; set; }
        [Required]
        [Display(Name = "Professional Qualification")]
        public string Professional { get; set; }
        [Required]
        [Display(Name = "Professional Qualification Description")]
        [DataType(DataType.MultilineText)]
        public string ProfessionalDescription { get; set; }

        [Required]
        [Display(Name = "Institution Attended")]
        [DataType(DataType.MultilineText)]
        public string Institution { get; set; }


        [Required]
        [Display(Name = "Duration")]
        public int Duration { get; set; }

        [Required]
        [Display(Name = "Start Date")]
        [DataType(DataType.Date)]
        public string StartDate { get; set; }

        [Required]
        [Display(Name = "Date Of Completion")]
        [DataType(DataType.Date)]
        public string EndDate { get; set; }

        [Required]
        [Display(Name = "Unit Head?")]
        public bool IsUnitHead { get; set; }

        [Required]
        [Display(Name = "Dept. Head?")]
        public bool IsDeptHead { get; set; }

        [Required]
        [Display(Name = "Regional Head?")]
        public bool IsRegionalHead { get; set; }

    }
}
