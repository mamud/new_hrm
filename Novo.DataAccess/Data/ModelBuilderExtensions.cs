﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;

namespace Novo.DataAccess.Data
{
    public static class ModelBuilderExtensions
    {
        private static string getPassword(string password)
        {

            

            var data = Encoding.ASCII.GetBytes(password);

            var md5 = new MD5CryptoServiceProvider();
            var md5data = md5.ComputeHash(data);

            var hashedPassword = Convert.ToBase64String(md5data);

            return hashedPassword;
        }
        public static void Seed(this ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<LeaveType>().HasData(
               new LeaveType
               {
                   Id = 1,
                   Type = "Examination"
               },
               new LeaveType
               {
                   Id = 2,
                   Type = "Maternity"
               },
               new LeaveType
               {
                   Id = 3,
                   Type = "Paternity"
               },
               new LeaveType
               {
                   Id = 4,
                   Type = "Sick"
               },
               new LeaveType
               {
                   Id = 5,
                   Type = "Annual"
               }
           );

            modelBuilder.Entity<Units>().HasData(
              new Units
              {
                  Id = 1,
                  Name = "Data",
                  Departments = "I.T"
              },
              new Units
              {
                  Id = 2,
                  Name = "I.T",
                  Departments = "I.T"
              },
              new Units
              {
                  Id = 3,
                  Name = "Client's Service",
                  Departments = "Client's Service"
              },
              new Units
              {
                  Id = 4,
                  Name = "Claims",
                  Departments = "Claims"
              },
              new Units
              {
                  Id = 5,
                  Name = "Account",
                  Departments = "Account"
              }
          );



            modelBuilder.Entity<ApplicationRole>().HasData(
               new ApplicationRole
               {
                   Id = 1,
                   Name = "HR",
                   NormalizedName = "HR"
               },
                new ApplicationRole
                {
                    Id = 2,
                    Name = "UnitHead",
                    NormalizedName = "UnitHead"
                },
                new ApplicationRole
                {
                    Id = 3,
                    Name = "Admin",
                    NormalizedName = "Admin"
                },
                new ApplicationRole
                {
                    Id = 4,
                    Name = "Super Admin",
                    NormalizedName = "Super Admin"
                },
                new ApplicationRole
                {
                    Id = 5,
                    Name = "User",
                    NormalizedName = "User"
                }
            );

           

            modelBuilder.Entity<ApplicationUser>().HasData(



            new ApplicationUser
               {
                   Id = 1,
                   UserName = "superadmin@hrms.com",
                   NormalizedUserName = "superadmin@hrms.com",
                   Email = "superadmin@hrms.com",
                   NormalizedEmail = "superadmin@hrms.com",
                   PhoneNumber = "00000000000",
                   FirstName = "Super",
                   LastName = "Admin",
                   PasswordHash = "Admin123*",
                   UnitsId = 2,
                   EmailConfirmed = true,
                   PhoneNumberConfirmed = true,
                   SecurityStamp = Guid.NewGuid().ToString("D")
               },
             
            new ApplicationUser
               {
                   Id = 2,
                   UserName = "fats@fat.com",
                   NormalizedUserName = "fats@fat.com",
                   Email = "fats@fat.com",
                   NormalizedEmail = "fats@fat.com",
                   PhoneNumber = "00000000000",
                   FirstName = "fats",
                   LastName = "fat",
                   PasswordHash = "Admin123*",
                   UnitsId = 2,
                   EmailConfirmed = true,
                   PhoneNumberConfirmed = true,
                   SecurityStamp = Guid.NewGuid().ToString("D")
               }

         
            );

            modelBuilder.Entity<LeaveUpload>().HasData(
                new LeaveUpload
                {
                    Id = 1,
                    Surname = "fat",
                    FirstName = "fats",
                    StaffNumber = null,
                    Location = null,
                    NumberOfOutstandingLeaves = "0",
                    CurrentLeaveDays = "20"
                }
                
            );



        }
    }
}
