﻿using Microsoft.AspNetCore.Identity;
using Novo.Models.Entity;
using NovoClients.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Novo.DataAccess.Data
{
    public static class DataSeedingInitializer
    {
        public static async Task UserAndRoleSeedAsync(ApplicationDbContext context, UserManager<IdentityUser> userManager,
                                                 RoleManager<IdentityRole> roleManager)
        {
            string[] roles = { "Admin", "Manager", "Staff" };
            foreach (var role in roles)
            {
                var roleExist = await roleManager.RoleExistsAsync(role);
                if (!roleExist)
                {
                    IdentityResult result = await roleManager.CreateAsync(new IdentityRole(role));
                }
            }

            //Create Admin User
            if (userManager.FindByEmailAsync("admin@payroll.com").Result == null)
            {
                IdentityUser user = new IdentityUser
                {
                    UserName = "admin@payroll.com",
                    Email = "admin@payroll.com"
                };
                IdentityResult identityResult = userManager.CreateAsync(user, "Password1").Result;
                if (identityResult.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Admin").Wait();
                }
            }

            //Create Manager User
            if (userManager.FindByEmailAsync("manager@payroll.com").Result == null)
            {
                IdentityUser user = new IdentityUser
                {
                    UserName = "manager@payroll.com",
                    Email = "manager@payroll.com"
                };
                IdentityResult identityResult = userManager.CreateAsync(user, "Password1").Result;
                if (identityResult.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Manager").Wait();
                }
            }

            //Create Staff User
            if (userManager.FindByEmailAsync("jane.doe@payroll.com").Result == null)
            {
                IdentityUser user = new IdentityUser
                {
                    UserName = "jane.doe@payroll.com",
                    Email = "jane.doe@payroll.com"
                };
                IdentityResult identityResult = userManager.CreateAsync(user, "Password1").Result;
                if (identityResult.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Staff").Wait();
                }
            }

            //Create No Role User
            if (userManager.FindByEmailAsync("john.doe@payroll.com").Result == null)
            {
                IdentityUser user = new IdentityUser
                {
                    UserName = "john.doe@payroll.com",
                    Email = "john.doe@payroll.com"
                };
                IdentityResult identityResult = userManager.CreateAsync(user, "Password1").Result;
                //No Role assigned to Mr John Doe
            }

            Units units = new Units
            {
                Id = 1,
                Name = "IT"
            };

            context.Units.Add(units);
            context.SaveChanges();
        }
    }
}
