﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class seedDataa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "ConcurrencyStamp", "CreatedDate", "Description", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 1, "d5953d9f-5993-4550-a68b-aa849bbda3c2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "HR", null },
                    { 2, "d21303c4-1caa-438c-81c6-75b0ed793f75", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "UnitHead", null },
                    { 3, "1da99ba8-de58-4698-9ecb-1cc8fbb54d83", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Admin", null },
                    { 4, "992347d1-524e-47bc-854a-77003a383ee9", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Super Admin", null }
                });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 122, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 122, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), "9/16/2020 2:46:40 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), "9/16/2020 2:46:40 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), "9/16/2020 2:46:40 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), "9/16/2020 2:46:40 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), "9/16/2020 2:46:40 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "9/16/2020 2:46:40 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "9/16/2020 2:46:40 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "9/16/2020 2:46:40 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "9/16/2020 2:46:40 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "9/16/2020 2:46:40 PM" });
        }
    }
}
