﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class addToLeaveUploads : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RemainingLeaves",
                table: "LeaveUploads",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TotalLeaves",
                table: "LeaveUploads",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "UtilizedLeaves",
                table: "LeaveUploads",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "46552597-59ec-43db-9d50-f772af4b6c98");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "44afd4fe-522e-408b-bc5d-b4975b35f9f9");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "65eeda6c-52db-4ce2-a01e-f700f8b20d7b");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "175633fb-b1e2-4013-8f42-0b81e5e64d0b");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "c1591e66-2a10-4146-a318-945d52b9f286");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "1ff409bb-fb54-4ffd-87a2-ca8f96ea82a5");

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RemainingLeaves",
                table: "LeaveUploads");

            migrationBuilder.DropColumn(
                name: "TotalLeaves",
                table: "LeaveUploads");

            migrationBuilder.DropColumn(
                name: "UtilizedLeaves",
                table: "LeaveUploads");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "a5182918-cb64-47dc-b1fa-ca2b0a6e5539");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "d84dedc3-9125-4d08-863d-b6edd9c3051f");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "e2ad60b9-09e3-431d-a8cf-d600f62703bb");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "da9e4713-9fa4-4415-8690-29512b555d14");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "4e079f7b-dcfe-4622-9279-778d44b36ec5");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "0f0b3e41-cca0-4832-9542-70dc46a012cb");

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });
        }
    }
}
