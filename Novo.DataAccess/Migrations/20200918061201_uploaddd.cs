﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class uploaddd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "9c4e5b00-1188-4472-b9fa-d3e4ef023bdd");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "faec07e7-6614-49e8-b238-6090e4ad283d");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "3edb1318-b556-443a-a135-90cd5564c5e3");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "e1833c34-a2cf-4d33-a1eb-ffd35fd87ae3");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "1dec6653-006f-4c3d-a191-1510e3fd7fb5");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "74e8cb22-93e1-4ebb-a6d1-a76191b6feb8", "yitGtJYIFfon8zShMpm1Ug==", "e3dcc22c-b3bd-4380-916a-18ccab3f032e" });

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "496961b8-8d72-4f06-abfe-605c1c8669f2", "yitGtJYIFfon8zShMpm1Ug==", "c6b0724a-fab1-4bea-96c0-d82fbae36851" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "44315d04-fdb8-4df9-b915-5a884ac81fc2");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "feacacde-3f5c-440e-9c13-303785ccfbb3");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "0c8a8b22-5b8f-4f1a-994b-a019a1e82e1a");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "9be343f6-0688-4435-93af-4370db2bba98");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "7648e8f9-07b9-4c8b-b1d2-d62fa6b20f0b");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "8d5a5510-3736-478b-b5ff-b78252ea6e70", "Admin123*", "e1e1bed7-911e-4d14-a986-e588bf921dce" });

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "3a6b1e0a-0415-4b6e-b2bb-6a43e0623cae", "Admin123*", "865cbfb1-36ca-4b2d-bb28-4cfb07c66db5" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 505, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 505, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });
        }
    }
}
