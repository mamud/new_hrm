﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class seedData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "LeaveTypes",
                columns: new[] { "Id", "AddedBy", "CreateDate", "Description", "IsDelete", "Type", "UpdateDate", "UpdatedBy" },
                values: new object[,]
                {
                    { 1, "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), null, false, "Examination", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), "9/16/2020 2:46:40 PM" },
                    { 2, "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), null, false, "Maternity", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), "9/16/2020 2:46:40 PM" },
                    { 3, "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), null, false, "Paternity", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), "9/16/2020 2:46:40 PM" },
                    { 4, "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), null, false, "Sick", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), "9/16/2020 2:46:40 PM" },
                    { 5, "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), null, false, "Annual", new DateTime(2020, 9, 16, 14, 46, 40, 507, DateTimeKind.Local), "9/16/2020 2:46:40 PM" }
                });

            migrationBuilder.InsertData(
                table: "Units",
                columns: new[] { "Id", "AddedBy", "CreateDate", "Departments", "IsDelete", "Name", "UpdateDate", "UpdatedBy" },
                values: new object[,]
                {
                    { 1, "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "I.T", false, "Data", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "9/16/2020 2:46:40 PM" },
                    { 2, "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "I.T", false, "I.T", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "9/16/2020 2:46:40 PM" },
                    { 3, "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "Client's Service", false, "Client's Service", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "9/16/2020 2:46:40 PM" },
                    { 4, "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "Claims", false, "Claims", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "9/16/2020 2:46:40 PM" },
                    { 5, "9/16/2020 2:46:40 PM", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "Account", false, "Account", new DateTime(2020, 9, 16, 14, 46, 40, 508, DateTimeKind.Local), "9/16/2020 2:46:40 PM" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5);
        }
    }
}
