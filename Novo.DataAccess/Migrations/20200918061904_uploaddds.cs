﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class uploaddds : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "8bd786d2-89c2-4d04-9455-87bc9c98d062");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "6051497c-0395-4a34-8ca9-132d5597fca3");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "2ce33b34-068a-4cf8-b9e2-5caf0f3e564f");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "01f4e544-5101-49ac-b418-8aefaff3214a");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "dc19f488-9941-443d-ba9c-8eac1b1a56dd");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "18bed3ec-be38-41b7-bb02-8e7f50baeeaa", "Admin123*", "3a191e83-75dd-4ec9-9ac7-1080c1238eb7" });

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "4f6462d7-f6be-4313-b2a4-49f232d3c0b1", "Admin123*", "7fb6f597-978e-4904-9fdb-6a93faa6a058" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:19:03 AM", new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), "9/18/2020 7:19:03 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:19:03 AM", new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), "9/18/2020 7:19:03 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:19:03 AM", new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), "9/18/2020 7:19:03 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:19:03 AM", new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), "9/18/2020 7:19:03 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:19:03 AM", new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), "9/18/2020 7:19:03 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:19:03 AM", new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), "9/18/2020 7:19:03 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:19:03 AM", new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), "9/18/2020 7:19:03 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:19:03 AM", new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), "9/18/2020 7:19:03 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:19:03 AM", new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), "9/18/2020 7:19:03 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:19:03 AM", new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 19, 3, 610, DateTimeKind.Local), "9/18/2020 7:19:03 AM" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "9c4e5b00-1188-4472-b9fa-d3e4ef023bdd");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "faec07e7-6614-49e8-b238-6090e4ad283d");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "3edb1318-b556-443a-a135-90cd5564c5e3");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "e1833c34-a2cf-4d33-a1eb-ffd35fd87ae3");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "1dec6653-006f-4c3d-a191-1510e3fd7fb5");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "74e8cb22-93e1-4ebb-a6d1-a76191b6feb8", "yitGtJYIFfon8zShMpm1Ug==", "e3dcc22c-b3bd-4380-916a-18ccab3f032e" });

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "496961b8-8d72-4f06-abfe-605c1c8669f2", "yitGtJYIFfon8zShMpm1Ug==", "c6b0724a-fab1-4bea-96c0-d82fbae36851" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 7:12:00 AM", new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), new DateTime(2020, 9, 18, 7, 12, 0, 900, DateTimeKind.Local), "9/18/2020 7:12:00 AM" });
        }
    }
}
