﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class addFormWeightAsInt : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            
            migrationBuilder.CreateTable(
                name: "Forms",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    AddedBy = table.Column<string>(nullable: true),
                    UpdatedBy = table.Column<string>(nullable: true),
                    CreateDate = table.Column<DateTime>(nullable: false),
                    UpdateDate = table.Column<DateTime>(nullable: false),
                    IsDelete = table.Column<bool>(nullable: false),
                    EmployeeId = table.Column<int>(nullable: false),
                    Question1 = table.Column<string>(nullable: true),
                    Weight1 = table.Column<int>(nullable: false),
                    Question2 = table.Column<string>(nullable: true),
                    Weight2 = table.Column<int>(nullable: false),
                    Question3 = table.Column<string>(nullable: true),
                    Weight3 = table.Column<int>(nullable: false),
                    Question4 = table.Column<string>(nullable: true),
                    Weight4 = table.Column<int>(nullable: false),
                    Question5 = table.Column<string>(nullable: true),
                    Weight5 = table.Column<int>(nullable: false),
                    Question6 = table.Column<string>(nullable: true),
                    Weight6 = table.Column<int>(nullable: false),
                    Question7 = table.Column<string>(nullable: true),
                    Weight7 = table.Column<int>(nullable: false),
                    Question8 = table.Column<string>(nullable: true),
                    Weight8 = table.Column<int>(nullable: false),
                    Question9 = table.Column<string>(nullable: true),
                    Weight9 = table.Column<int>(nullable: false),
                    Question10 = table.Column<string>(nullable: true),
                    Weight10 = table.Column<int>(nullable: false),
                    Question11 = table.Column<string>(nullable: true),
                    Weight11 = table.Column<int>(nullable: false),
                    Question12 = table.Column<string>(nullable: true),
                    Weight12 = table.Column<int>(nullable: false),
                    Question13 = table.Column<string>(nullable: true),
                    Weight13 = table.Column<int>(nullable: false),
                    Question14 = table.Column<string>(nullable: true),
                    Weight14 = table.Column<int>(nullable: false),
                    Question15 = table.Column<string>(nullable: true),
                    Weight15 = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Forms", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
           

            migrationBuilder.DropTable(
                name: "Forms");
        }
    }
}
