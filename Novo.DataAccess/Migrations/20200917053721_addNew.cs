﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class addNew : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "a5182918-cb64-47dc-b1fa-ca2b0a6e5539");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "d84dedc3-9125-4d08-863d-b6edd9c3051f");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "e2ad60b9-09e3-431d-a8cf-d600f62703bb");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "da9e4713-9fa4-4415-8690-29512b555d14");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "4e079f7b-dcfe-4622-9279-778d44b36ec5");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "0f0b3e41-cca0-4832-9542-70dc46a012cb");

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 6:37:20 AM", new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), new DateTime(2020, 9, 17, 6, 37, 20, 76, DateTimeKind.Local), "9/17/2020 6:37:20 AM" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "00f3fc2a-f2cd-4c87-995d-04e45f659588");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "751d447c-4e8d-4182-8fb6-42213eae5e51");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "d2c96470-01e7-40c0-ab58-150068579b2e");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "4d7779a6-9c5c-45b3-9c15-bcd504889509");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "2244f2fb-4ce0-4ed5-84f9-c16263e2da11");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "bcf5a25d-a388-4518-825f-0757412175f0");

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 707, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 707, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });
        }
    }
}
