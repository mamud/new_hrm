﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class uploadd : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "44315d04-fdb8-4df9-b915-5a884ac81fc2");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "feacacde-3f5c-440e-9c13-303785ccfbb3");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "0c8a8b22-5b8f-4f1a-994b-a019a1e82e1a");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "9be343f6-0688-4435-93af-4370db2bba98");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "7648e8f9-07b9-4c8b-b1d2-d62fa6b20f0b");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "EmailConfirmed", "PhoneNumberConfirmed", "SecurityStamp" },
                values: new object[] { "8d5a5510-3736-478b-b5ff-b78252ea6e70", true, true, "e1e1bed7-911e-4d14-a986-e588bf921dce" });

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "EmailConfirmed", "PhoneNumberConfirmed", "SecurityStamp" },
                values: new object[] { "3a6b1e0a-0415-4b6e-b2bb-6a43e0623cae", true, true, "865cbfb1-36ca-4b2d-bb28-4cfb07c66db5" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 505, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 505, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:45:19 AM", new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 45, 19, 506, DateTimeKind.Local), "9/18/2020 6:45:19 AM" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "81a2ef0e-5fc8-4fbd-8c12-74f5820fb97f");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "a36206a4-86f3-47cd-a759-28fb35c4dfb6");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "7acbbabc-ca12-4d09-a7ee-908766600c26");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "09b0683f-0fe2-4fc2-8561-6ad6fa325837");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "9e7d71d7-03d7-4e0c-b9ec-d834ecf8c123");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "EmailConfirmed", "PhoneNumberConfirmed", "SecurityStamp" },
                values: new object[] { "e977831d-95db-43f1-b044-f7f39986a372", false, false, "d1bdc37a-f0a1-4536-bf92-5abeeb0fa4ab" });

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "EmailConfirmed", "PhoneNumberConfirmed", "SecurityStamp" },
                values: new object[] { "f3c546a9-e213-4ed2-b82c-896f4eab4fd1", false, false, "4b2e8072-1a4d-4c42-a462-923f3a4668aa" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 745, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 745, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });
        }
    }
}
