﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class addToLeaveUploads_ToString : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "UtilizedLeaves",
                table: "LeaveUploads",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "TotalLeaves",
                table: "LeaveUploads",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<string>(
                name: "RemainingLeaves",
                table: "LeaveUploads",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "b6429693-8925-4788-8025-9f7e8c0d5efa");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "a4e7b220-3d5c-487e-a038-42d13a0073dc");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "bda42fa1-a8ca-4538-a05e-a0879e207e98");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "c8e78e9e-30e3-44bf-8219-18d55d563868");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "0aa9ac15-a2a4-49df-ba58-b447b8ed338b");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "51f47ca4-55c6-406b-a905-99d267878ff3");

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 344, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 344, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "UtilizedLeaves",
                table: "LeaveUploads",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "TotalLeaves",
                table: "LeaveUploads",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "RemainingLeaves",
                table: "LeaveUploads",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "46552597-59ec-43db-9d50-f772af4b6c98");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "44afd4fe-522e-408b-bc5d-b4975b35f9f9");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "65eeda6c-52db-4ce2-a01e-f700f8b20d7b");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "175633fb-b1e2-4013-8f42-0b81e5e64d0b");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "c1591e66-2a10-4146-a318-945d52b9f286");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "1ff409bb-fb54-4ffd-87a2-ca8f96ea82a5");

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 339, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 12:24:08 PM", new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), new DateTime(2020, 9, 17, 12, 24, 8, 340, DateTimeKind.Local), "9/17/2020 12:24:08 PM" });
        }
    }
}
