﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class upload : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "81a2ef0e-5fc8-4fbd-8c12-74f5820fb97f");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "a36206a4-86f3-47cd-a759-28fb35c4dfb6");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "7acbbabc-ca12-4d09-a7ee-908766600c26");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "09b0683f-0fe2-4fc2-8561-6ad6fa325837");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "9e7d71d7-03d7-4e0c-b9ec-d834ecf8c123");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "NormalizedEmail", "NormalizedUserName" },
                values: new object[] { "e977831d-95db-43f1-b044-f7f39986a372", "superadmin@hrms.com", "superadmin@hrms.com" });

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "NormalizedEmail", "NormalizedUserName" },
                values: new object[] { "f3c546a9-e213-4ed2-b82c-896f4eab4fd1", "fats@fat.com", "fats@fat.com" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 745, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 745, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:36:31 AM", new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 36, 31, 746, DateTimeKind.Local), "9/18/2020 6:36:31 AM" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "3ac20d34-18aa-4d60-a4a9-1db0ce3da91f");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "08691e2e-b07c-415a-a3dd-93e396d7e327");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "7b2dc686-aa9d-45f5-8eec-150469005fb7");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "9a2a6f11-54ee-4c82-a513-9f28401bd54a");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "5808acec-4ba8-4b04-a322-c60adfab462d");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "NormalizedEmail", "NormalizedUserName" },
                values: new object[] { "1d9b0c79-5083-4670-96ae-b44c98337c10", null, null });

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "NormalizedEmail", "NormalizedUserName" },
                values: new object[] { "52177628-449a-43c9-a3bc-65aa52d64bbd", null, null });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });
        }
    }
}
