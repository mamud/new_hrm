﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class seesdData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.InsertData(
                table: "AppRole",
                columns: new[] { "Id", "ConcurrencyStamp", "CreatedDate", "Description", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 1, "f1098331-a833-4887-8f1a-fb1f337a277d", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "HR", null },
                    { 2, "3a7499e3-24c4-4659-abc6-7c7ff46f5953", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "UnitHead", null },
                    { 3, "6487eee9-4033-4c36-9c9d-882e599f69e0", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Admin", null },
                    { 4, "49264c70-6abe-42e6-b922-54b88d745f7d", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Super Admin", null }
                });

            migrationBuilder.InsertData(
                table: "Employee",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UnitsId", "UserName" },
                values: new object[] { 1, 0, "d1bdc37a-f0a1-4536-bf92-5abeeb0fa4ab", "superadmin@hrms.com", false, "Super", "Admin", false, null, null, null, "P@Admin123*", "00000000000", false, null, false, 4, "superadmin@hrms.com" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 597, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 597, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AppRole",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { 1, "f1098331-a833-4887-8f1a-fb1f337a277d" });

            migrationBuilder.DeleteData(
                table: "AppRole",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { 2, "3a7499e3-24c4-4659-abc6-7c7ff46f5953" });

            migrationBuilder.DeleteData(
                table: "AppRole",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { 3, "6487eee9-4033-4c36-9c9d-882e599f69e0" });

            migrationBuilder.DeleteData(
                table: "AppRole",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { 4, "49264c70-6abe-42e6-b922-54b88d745f7d" });

            migrationBuilder.DeleteData(
                table: "Employee",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { 1, "d1bdc37a-f0a1-4536-bf92-5abeeb0fa4ab" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 121, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "ConcurrencyStamp", "CreatedDate", "Description", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { 1, "d5953d9f-5993-4550-a68b-aa849bbda3c2", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "HR", null },
                    { 2, "d21303c4-1caa-438c-81c6-75b0ed793f75", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "UnitHead", null },
                    { 3, "1da99ba8-de58-4698-9ecb-1cc8fbb54d83", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Admin", null },
                    { 4, "992347d1-524e-47bc-854a-77003a383ee9", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "Super Admin", null }
                });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 122, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 122, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:00:40 PM", new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 0, 40, 123, DateTimeKind.Local), "9/16/2020 3:00:40 PM" });
        }
    }
}
