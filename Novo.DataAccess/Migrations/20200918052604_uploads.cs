﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class uploads : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "3ac20d34-18aa-4d60-a4a9-1db0ce3da91f");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "08691e2e-b07c-415a-a3dd-93e396d7e327");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "7b2dc686-aa9d-45f5-8eec-150469005fb7");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "9a2a6f11-54ee-4c82-a513-9f28401bd54a");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "5808acec-4ba8-4b04-a322-c60adfab462d");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "UnitsId" },
                values: new object[] { "1d9b0c79-5083-4670-96ae-b44c98337c10", 2 });

            migrationBuilder.InsertData(
                table: "Employee",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "FirstName", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UnitsId", "UserName" },
                values: new object[] { 2, 0, "52177628-449a-43c9-a3bc-65aa52d64bbd", "fats@fat.com", false, "fats", "fat", false, null, null, null, "Admin123*", "00000000000", false, "4b2e8072-1a4d-4c42-a462-923f3a4668aa", false, 2, "fats@fat.com" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 91, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.InsertData(
                table: "LeaveUploads",
                columns: new[] { "Id", "CurrentLeaveDays", "FirstName", "Location", "NumberOfOutstandingLeaves", "RemainingLeaves", "StaffNumber", "Surname", "TotalLeaves", "UtilizedLeaves" },
                values: new object[] { 1, "20", "fats", null, "0", null, null, "fat", null, null });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/18/2020 6:26:03 AM", new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), new DateTime(2020, 9, 18, 6, 26, 3, 92, DateTimeKind.Local), "9/18/2020 6:26:03 AM" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Employee",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { 2, "52177628-449a-43c9-a3bc-65aa52d64bbd" });

            migrationBuilder.DeleteData(
                table: "LeaveUploads",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "b6429693-8925-4788-8025-9f7e8c0d5efa");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "a4e7b220-3d5c-487e-a038-42d13a0073dc");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "bda42fa1-a8ca-4538-a05e-a0879e207e98");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "c8e78e9e-30e3-44bf-8219-18d55d563868");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 5,
                column: "ConcurrencyStamp",
                value: "0aa9ac15-a2a4-49df-ba58-b447b8ed338b");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "UnitsId" },
                values: new object[] { "51f47ca4-55c6-406b-a905-99d267878ff3", 4 });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 344, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 344, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/17/2020 1:27:36 PM", new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), new DateTime(2020, 9, 17, 13, 27, 36, 345, DateTimeKind.Local), "9/17/2020 1:27:36 PM" });
        }
    }
}
