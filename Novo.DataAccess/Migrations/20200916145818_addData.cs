﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class addData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "00f3fc2a-f2cd-4c87-995d-04e45f659588");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "751d447c-4e8d-4182-8fb6-42213eae5e51");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "d2c96470-01e7-40c0-ab58-150068579b2e");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "4d7779a6-9c5c-45b3-9c15-bcd504889509");

            migrationBuilder.InsertData(
                table: "AppRole",
                columns: new[] { "Id", "ConcurrencyStamp", "CreatedDate", "Description", "Name", "NormalizedName" },
                values: new object[] { 5, "2244f2fb-4ce0-4ed5-84f9-c16263e2da11", new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), null, "User", "User" });

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "bcf5a25d-a388-4518-825f-0757412175f0");

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 707, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 707, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 708, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:58:17 PM", new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 58, 17, 709, DateTimeKind.Local), "9/16/2020 3:58:17 PM" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AppRole",
                keyColumns: new[] { "Id", "ConcurrencyStamp" },
                keyValues: new object[] { 5, "2244f2fb-4ce0-4ed5-84f9-c16263e2da11" });

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "ee40fd10-750d-48c7-95fe-c5a8936f7677");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                column: "ConcurrencyStamp",
                value: "0bf04bda-181f-4328-bd9d-4cb9c1f00d16");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                column: "ConcurrencyStamp",
                value: "98fb06ae-52e1-4c3b-9424-e3dae4574138");

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                column: "ConcurrencyStamp",
                value: "8b4d105d-87da-4b12-95cb-f7c86d76c34e");

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                column: "ConcurrencyStamp",
                value: "7a1f998a-6ed5-45a5-a711-1211385ff6d8");

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 122, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 122, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });
        }
    }
}
