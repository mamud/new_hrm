﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Novo.DataAccess.Migrations
{
    public partial class seesdDataa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "NormalizedName" },
                values: new object[] { "ee40fd10-750d-48c7-95fe-c5a8936f7677", "HR" });

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "NormalizedName" },
                values: new object[] { "0bf04bda-181f-4328-bd9d-4cb9c1f00d16", "UnitHead" });

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "ConcurrencyStamp", "NormalizedName" },
                values: new object[] { "98fb06ae-52e1-4c3b-9424-e3dae4574138", "Admin" });

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "ConcurrencyStamp", "NormalizedName" },
                values: new object[] { "8b4d105d-87da-4b12-95cb-f7c86d76c34e", "Super Admin" });

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "7a1f998a-6ed5-45a5-a711-1211385ff6d8", "Admin123*", "d1bdc37a-f0a1-4536-bf92-5abeeb0fa4ab" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 122, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 122, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 123, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:46:31 PM", new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 46, 31, 124, DateTimeKind.Local), "9/16/2020 3:46:31 PM" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "NormalizedName" },
                values: new object[] { "f1098331-a833-4887-8f1a-fb1f337a277d", null });

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "ConcurrencyStamp", "NormalizedName" },
                values: new object[] { "3a7499e3-24c4-4659-abc6-7c7ff46f5953", null });

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "ConcurrencyStamp", "NormalizedName" },
                values: new object[] { "6487eee9-4033-4c36-9c9d-882e599f69e0", null });

            migrationBuilder.UpdateData(
                table: "AppRole",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "ConcurrencyStamp", "NormalizedName" },
                values: new object[] { "49264c70-6abe-42e6-b922-54b88d745f7d", null });

            migrationBuilder.UpdateData(
                table: "Employee",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "ConcurrencyStamp", "PasswordHash", "SecurityStamp" },
                values: new object[] { "d1bdc37a-f0a1-4536-bf92-5abeeb0fa4ab", "P@Admin123*", null });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 597, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 597, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "LeaveTypes",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 598, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 2,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 3,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 4,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });

            migrationBuilder.UpdateData(
                table: "Units",
                keyColumn: "Id",
                keyValue: 5,
                columns: new[] { "AddedBy", "CreateDate", "UpdateDate", "UpdatedBy" },
                values: new object[] { "9/16/2020 3:17:01 PM", new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), new DateTime(2020, 9, 16, 15, 17, 1, 599, DateTimeKind.Local), "9/16/2020 3:17:01 PM" });
        }
    }
}
