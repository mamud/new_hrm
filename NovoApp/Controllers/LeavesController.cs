﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MimeKit;
using MimeKit.Text;
using Novo.Models.Entity;
using NovoClients.DataAccess.Data;
using MailKit.Net.Smtp;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;
using NovoApp.Utility;

namespace NovoApp.Controllers
{
    public class LeavesController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public LeavesController(ApplicationDbContext context, 
                                SignInManager<ApplicationUser> signInManager, 
                                UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _context = context;
        }

        // GET: Leaves
        [Authorize(Roles = "User")]
        public async Task<IActionResult> Index()
        {
            var leave = _context.LeaveTypes.GetType();
            var user = await _userManager.GetUserAsync(User);
            return View(await _context.Leaves.Where(x => x.EmployeeEmail == user.Email).ToListAsync());
        }

        
        public IActionResult Error()
        {
           
            return View();
        }

        [Authorize(Roles = "User")]
        public async Task<IActionResult> Display()
        {
            var leave = _context.LeaveTypes.GetType();
            var user = await _userManager.GetUserAsync(User);
            var result = await _context.Leaves.Where(x => x.EmployeeEmail == user.Email).ToListAsync();
            return new JsonResult(result);
        }

        [Authorize(Roles = "HR, HRHead, Admin, SuperAdmin")]
        public async Task<IActionResult> Leaves()
        {

            var user = await _userManager.GetUserAsync(User);

            ViewBag.AwaitingApproval = _context.Leaves.Where(x => x.isUnitHeadApproved == true && x.IsDelete == false && x.isHrApproved == false).Count();
            ViewBag.Disapproved = _context.Leaves.Where(x => x.isUnitHeadDisapprove == true && x.IsDelete == false && x.isHrApproved == false).Count();
            ViewBag.Approved = _context.Leaves.Where(x => x.isUnitHeadApproved == true && x.IsDelete == false && x.isHrApproved == true).Count();
            var Leaves = _context.Leaves.Where(x => x.isUnitHeadApproved == true || x.isUnitHeadDisapprove == true && x.IsDelete == false).ToList();

            return View(Leaves);
        }

        [Authorize(Roles = "UnitHead, DeptHead, Admin, SuperAdmin")]
        public async Task<IActionResult> LeavesList()
        {

            var user = await _userManager.GetUserAsync(User);
            

            ViewBag.AwaitingHrApproval = _context.Leaves.Where(x => x.isUnitHeadApproved == true && x.IsDelete == false && x.isHrApproved == false).Count();
            ViewBag.Approved = _context.Leaves.Where(x => x.isUnitHeadApproved == true && x.IsDelete == false && x.isHrApproved == true).Count();
            
            var LeavesList = _context.Leaves.Where(x => x.IsDelete == false && x.UnitsId == user.UnitsId).ToList();
            
            
            return View(LeavesList);
        }


     

        // GET: Leaves/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leaves = await _context.Leaves
                .Include(l => l.LeaveType)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (leaves == null)
            {
                return NotFound();
            }

            return View(leaves);
        }

        // GET: Leaves/Create
        public async Task<IActionResult> Create()
        {
            var user = await _userManager.GetUserAsync(User);

            ViewData["LeaveTypeId"] = new SelectList(_context.LeaveTypes, "Id", "Type");
            ViewData["UnitsId"] = new SelectList(_context.Units, "Id", "Name");

            var leaveUpload = _context.LeaveUploads.Where(x => x.FirstName.ToLower() == user.FirstName.ToLower() ||
                                            x.FirstName.ToLower() == user.LastName.ToLower());

            var remainingLeaves = leaveUpload.Select(x => x.RemainingLeaves).FirstOrDefault();

            if (Convert.ToInt32(remainingLeaves) <= 0 || remainingLeaves == null)
            {
                return RedirectToAction(nameof(Error));
            }

            ViewBag.RemainingLeaves = remainingLeaves;

            ViewBag.CurrentLeaves = Int32.Parse(leaveUpload.Select(x => x.CurrentLeaveDays).FirstOrDefault());

            //ViewBag.CurrentLeaves = Int32.Parse(_context.LeaveUploads.Where(x => x.FirstName.ToLower() == user.FirstName.ToLower() || x.FirstName.ToLower() == user.LastName.ToLower()).Select(x => x.CurrentLeaveDays).FirstOrDefault());
            //ViewBag.Utilized = _context.Leaves.Where(x => x.EmployeeEmail == user.Email).Count();

            //ViewBag.Utilized = Int32.Parse(leaveUpload.Select(x => x.UtilizedLeaves).FirstOrDefault()) ?? "0";

            var utilized = leaveUpload.Select(x => x.UtilizedLeaves).FirstOrDefault();

            if (utilized == null)
            {
                ViewBag.Utilized = 0;
            }
            else
            {
                ViewBag.Utilized = utilized;
            }

            ViewBag.OutstandingLeaves = Int32.Parse(leaveUpload.Select(x => x.NumberOfOutstandingLeaves).FirstOrDefault());

            ViewBag.TotalLeaves = Int32.Parse(leaveUpload.Select(x => x.TotalLeaves).FirstOrDefault());

            //if (_context.LeaveUploads.Where(x => x.FirstName.ToLower() == user.FirstName.ToLower() || 
            //                        x.FirstName.ToLower() == user.LastName.ToLower()).Select(x => x.NumberOfOutstandingLeaves).FirstOrDefault() == String.Empty)
            //{
            //    ViewBag.OutstandingLeaves = 0;
            //}
            //else
            //{
            //    ViewBag.OutstandingLeaves = Int32.Parse(_context.LeaveUploads.Where(x => x.FirstName == user.FirstName ||
            //                                x.FirstName == user.LastName).Select(x => x.NumberOfOutstandingLeaves).FirstOrDefault());
            //}                                                                   

            //ViewBag.ToEmail = user.FirstName + ' ' + user.LastName;           


            //var eee = (from a in _context.Users                               
            //           join b in _context.UserRoles on a.Id equals b.UserId   
            //           join c in _context.Roles on b.RoleId equals c.Id       
            //           where a.Id == b.UserId && c.Name == "UnitHead" && a.Un 
            //           select new                                             
            //           {                                                      
            //               a.Email,                                           
            //               a.FirstName,                                       
            //               a.LastName                                         

            //           }).FirstOrDefault();

            //ViewBag.FromEmail = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();

            return View();
        }

        private static int NumberOfWeeekends(DateTime startDate, DateTime endDate)
        {
            int countWeekend = 0;



            TimeSpan diff = endDate - startDate;
            int days = diff.Days;
            for (var i = 0; i <= days; i++)
            {
                var testDate = startDate.AddDays(i);
                switch (testDate.DayOfWeek)
                {
                    case DayOfWeek.Saturday:
                    case DayOfWeek.Sunday:
                        //Console.WriteLine(testDate.ToShortDateString());
                        countWeekend++;
                        break;
                }

            }

            return countWeekend;
        }
        public IActionResult RequestNotProcessed()
        {

            return View();
        }

        // POST: Leaves/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("StartDate,EndDate,EmployeeEmail,EmployeeId,LeaveTypeId,Purpose,isUnitHeadApproved,isHrApproved,TotalLeave,LeaveUtilized,LeaveRemaining,LeaveRequestStatus,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete,HandOver,UnitsId")] Leaves leaves)
        {
            if (ModelState.IsValid || !ModelState.IsValid)
            {

                var user = await _userManager.GetUserAsync(User);
                var startDate = Convert.ToDateTime(leaves.StartDate);
                var endDate = Convert.ToDateTime(leaves.EndDate);

                int getWeekend = NumberOfWeeekends(startDate, endDate);
                var leaveRequested = (endDate - startDate).Days - getWeekend;

                var leaveUpload = _context.LeaveUploads.Where(x => x.FirstName.ToLower() == user.FirstName.ToLower() ||
                                            x.FirstName.ToLower() == user.LastName.ToLower()).FirstOrDefault();

                leaveUpload.RemainingLeaves = (Convert.ToInt32(leaveUpload.RemainingLeaves) - Convert.ToInt32(leaveRequested)).ToString();
                leaveUpload.UtilizedLeaves = (Convert.ToInt32(leaveUpload.UtilizedLeaves) + Convert.ToInt32(leaveRequested)).ToString();

                if (Convert.ToInt32(leaveRequested) > Convert.ToInt32(leaveUpload.RemainingLeaves))
                {
                    return RedirectToAction(nameof(RequestNotProcessed));
                }

                _context.Update(leaveUpload);

                //var dd = new Leaves
                //{
                //    StartDate = leaves.StartDate,
                //    EndDate = leaves.EndDate,
                //    EmployeeEmail = leaves.EmployeeEmail,
                //    EmployeeId = leaves.EmployeeId,
                //    LeaveTypeId = leaves.LeaveTypeId,
                //    Purpose = leaves.Purpose,
                //    isUnitHeadApproved = leaves.isUnitHeadApproved,
                //    isHrApproved = leaves.isHrApproved,
                //    TotalLeave = leaves.TotalLeave,
                //    LeaveUtilized = leaves.LeaveUtilized,
                //    LeaveRemaining = leaves.LeaveRemaining,
                //    LeaveRequestStatus = leaves.LeaveRequestStatus,
                //    UnitsId = leaves.UnitsId,
                //    AddedBy = leaves.AddedBy,
                //    UpdatedBy = leaves.UpdatedBy,
                //    CreateDate = leaves.CreateDate,
                //    UpdateDate = leaves.UpdateDate,
                //    HandOver = leaves.HandOver
                //};

                

                _context.Add(leaves);
                await _context.SaveChangesAsync();
                //var user = await _userManager.GetUserAsync(User);
                ViewBag.ToEmail = user.Email;
                var fromEmail = user.Email;
                var fromName = user.FirstName + ' ' + user.LastName;

                var eee = (from a in _context.Users
                           join b in _context.UserRoles on a.Id equals b.UserId
                           join c in _context.Roles on b.RoleId equals c.Id
                           where a.Id == b.UserId && c.Name == "UnitHead" && a.UnitsId == user.UnitsId
                           select new
                           {
                               a.Email,
                               a.FirstName,
                               a.LastName

                           }).FirstOrDefault();

                ViewBag.FromEmail = eee.Email.ToString();
                var toEmail = eee.Email.ToString();
                var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();

                
                //instantiate a new MimeMessage
                var message = new MimeMessage();
                    //Setting the To e-mail address
                    message.To.Add(new MailboxAddress(toName, toEmail));

                   
                    var tt = "humanresources@novohealthafrica.org";
                    message.Bcc.Add(new MailboxAddress(tt));
                    //Setting the From e-mail address
                    message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                    //E-mail subject 
                    message.Subject = "New Service Request Added";
                    //E-mail message body
                    message.Body = new TextPart(TextFormat.Html)
                    {
                        Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                       "<br/><br/><br/><br/>" +
                    "<p>Dear Colleague,</p>" +

                   "<p> This is to notify you that a new leave request has been sent by " + user.FirstName + " " + user.LastName +

                   "<br/>on </a>" + leaves.CreateDate + ".</p>" +
                   "Unit Head can see more information on the leave request by Clicking <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +




                                                  "<br/>Regards<br/>" +


                                                   "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                    };

                    //Configure the e-mail
                    //using (var emailClient = new SmtpClient())
                    //{

                    //    emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                    //    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    //    emailClient.Connect("smtp.office365.com", 587, false);
                    //    emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                    //    emailClient.Send(message);
                    //    emailClient.Disconnect(true);
                    //}
                

                return RedirectToAction(nameof(Index));
            }
            ViewData["LeaveTypeId"] = new SelectList(_context.LeaveTypes, "Id", "Type", leaves.LeaveTypeId);
            ViewData["UnitsId"] = new SelectList(_context.Units, "Id", "Name", leaves.UnitsId);
            return RedirectToAction("Create", "LeaveHandovers");
        }



        // GET: Leaves/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }


            var leaves = await _context.Leaves.FindAsync(id);
            if (leaves == null)
            {
                return NotFound();
            }
            
            ViewData["LeaveTypeId"] = new SelectList(_context.LeaveTypes, "Id", "Type", leaves.LeaveTypeId);
            ViewData["UnitsId"] = new SelectList(_context.Units, "Id", "Name", leaves.UnitsId);
            return View(leaves);
        }

        // POST: Leaves/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("StartDate,EndDate,EmployeeEmail,LeaveTypeId,Purpose,isUnitHeadApproved,isHrApproved,TotalLeave,LeaveUtilized,LeaveRemaining,LeaveRequestStatus,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete,HandOver,UnitsId,EmployeeId,isUnitHeadDisapprove,isHrDisapprove")] Leaves leaves)
        {
            if (id != leaves.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(leaves);
                    await _context.SaveChangesAsync();
                    if (leaves.isUnitHeadApproved == true && leaves.isHrApproved == true)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        ViewBag.ToEmail = user.Email;
                        var fromEmail = user.Email;
                        var fromName = user.FirstName + ' ' + user.LastName;

                        var eee = (from a in _context.Users
                                   join b in _context.UserRoles on a.Id equals b.UserId
                                   join c in _context.Roles on b.RoleId equals c.Id
                                   where a.Id == b.UserId && c.Name == "UnitHead" && a.UnitsId == user.UnitsId
                                   select new
                                   {
                                       a.Email,
                                       a.FirstName,
                                       a.LastName

                                   }).FirstOrDefault();

                        ViewBag.FromEmail = eee.Email.ToString();

                        //var use = Int32.Parse(_context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeId).LastOrDefault().ToString());


                        //var uuuu = _context.Users.Find(use);

                        //var ttyt = uuuu.Email.ToString();
                        //var tttt = uuuu.FirstName.ToString() + ' ' + uuuu.LastName.ToString(); 
                        var use = _context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeEmail).FirstOrDefault();


                        //var tttt = _context.Users.Find(use).Email.ToString();
                        
                        
                        var toEmail = eee.Email.ToString();
                        var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();

                        //var ffff = _context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeId);

                        //instantiate a new MimeMessage
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(use));

                        //message.Bcc.Add(new MailboxAddress(CompanyEmail));
                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Service Request Added";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {

                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear</p>" + leaves.AddedBy + "," +

                       "<p> This is to notify you that the leave request you sent on " + leaves.CreateDate + "has been approved by your Unit Head and the HR." +
                       "For more inforation on the leave request, Click <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +



                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"

                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (leaves.isUnitHeadApproved == true && leaves.isHrApproved == false)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        ViewBag.ToEmail = user.Email;
                        var fromEmail = user.Email;
                        var fromName = user.FirstName + ' ' + user.LastName;

                        var eee = (from a in _context.Users
                                   join b in _context.UserRoles on a.Id equals b.UserId
                                   join c in _context.Roles on b.RoleId equals c.Id
                                   where a.Id == b.UserId && c.Name == "UnitHead" && a.UnitsId == user.UnitsId
                                   select new
                                   {
                                       a.Email,
                                       a.FirstName,
                                       a.LastName

                                   }).FirstOrDefault();

                        ViewBag.FromEmail = eee.Email.ToString();

                        var use = _context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeEmail).FirstOrDefault();

                        var toEmail = eee.Email.ToString();
                        var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();

                        //instantiate a new MimeMessage
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(use));

                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Service Request Added";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear</p>" + leaves.AddedBy + "," +

                       "<p> This is to notify you that the leave request you sent on " + leaves.CreateDate + "has been approved by your Unit Head but rejected by the Head of Human Resource Unit." +
                      "For more inforation on the leave request, Click <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"

                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (leaves.isUnitHeadApproved == true)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        ViewBag.ToEmail = user.Email;
                        var fromEmail = user.Email;
                        var fromName = user.FirstName + ' ' + user.LastName;

                        var eee = (from a in _context.Users
                                   join b in _context.UserRoles on a.Id equals b.UserId
                                   join c in _context.Roles on b.RoleId equals c.Id
                                   where a.Id == b.UserId && c.Name == "HR"
                                   select new
                                   {
                                       a.Email,
                                       a.FirstName,
                                       a.LastName

                                   }).FirstOrDefault();

                        ViewBag.FromEmail = eee.Email.ToString();
                        var toEmail = eee.Email.ToString();
                        var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();


                        //instantiate a new MimeMessage
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(toName, toEmail));

                        //message.Bcc.Add(new MailboxAddress(CompanyEmail));
                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Leave Request";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear</p>" + leaves.AddedBy + "," +

                       "<p> This is to notify you that the leave request you sent on " + leaves.CreateDate + "has been approved by your Unit Head but awaiting approval by the Head of Human Resource Unit." +
                       "For more inforation on the leave request, Click <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +





                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"

                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (leaves.isUnitHeadApproved == false)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        ViewBag.ToEmail = user.Email;
                        var fromEmail = user.Email;
                        var fromName = user.FirstName + ' ' + user.LastName;

                        var eee = (from a in _context.Users
                                   join b in _context.UserRoles on a.Id equals b.UserId
                                   join c in _context.Roles on b.RoleId equals c.Id
                                   where a.Id == b.UserId && c.Name == "UnitHead" && a.UnitsId == user.UnitsId
                                   select new
                                   {
                                       a.Email,
                                       a.FirstName,
                                       a.LastName
                                   }).FirstOrDefault();

                        ViewBag.FromEmail = eee.Email.ToString();

                        var use = _context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeEmail).FirstOrDefault();

                        var toEmail = eee.Email.ToString();
                        var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();

                        //instantiate a new MimeMessage
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(use));

                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Service Request Added";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear</p>" + leaves.AddedBy + "," +

                       "<p> This is to notify you that the leave request you sent on " + leaves.CreateDate + "has been rejected by your Unit Head." +
                      "For more inforation on the leave request, Click <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +



                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"

                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {
                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LeavesExists(leaves.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }


                if(_signInManager.IsSignedIn(User) && User.IsInRole("UnitHead"))
                {
                    return RedirectToAction("leaveslist","leaves");
                }
                else if (_signInManager.IsSignedIn(User) && User.IsInRole("HR"))
                {
                    return RedirectToAction("leaves", "leaves");
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["LeaveTypeId"] = new SelectList(_context.LeaveTypes, "Id", "Type", leaves.LeaveTypeId);
            ViewData["UnitsId"] = new SelectList(_context.Units, "Id", "Name", leaves.UnitsId);
            return View(leaves);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddOrEdit(int id, [Bind("StartDate,EndDate,EmployeeEmail,LeaveTypeId,Purpose,isUnitHeadApproved,isHrApproved,TotalLeave,LeaveUtilized,LeaveRemaining,LeaveRequestStatus,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete,HandOver,UnitsId,EmployeeId,isUnitHeadDisapprove,isHrDisapprove")] Leaves leaves)
        {
            if (id == 0)
            {
                    _context.Add(leaves);
                    await _context.SaveChangesAsync();
                    var user = await _userManager.GetUserAsync(User);
                    ViewBag.ToEmail = user.Email;
                    var fromEmail = user.Email;
                    var fromName = user.FirstName + ' ' + user.LastName;

                    var eee = (from a in _context.Users
                               join b in _context.UserRoles on a.Id equals b.UserId
                               join c in _context.Roles on b.RoleId equals c.Id
                               where a.Id == b.UserId && c.Name == "UnitHead" && a.UnitsId == user.UnitsId
                               select new
                               {
                                   a.Email,
                                   a.FirstName,
                                   a.LastName

                               }).FirstOrDefault();

                    ViewBag.FromEmail = eee.Email.ToString();
                    var toEmail = eee.Email.ToString();
                    var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();


                    //instantiate a new MimeMessage
                    var message = new MimeMessage();
                    //Setting the To e-mail address
                    message.To.Add(new MailboxAddress(toName, toEmail));


                    var tt = "humanresources@novohealthafrica.org";
                    message.Bcc.Add(new MailboxAddress(tt));
                    //Setting the From e-mail address
                    message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                    //E-mail subject 
                    message.Subject = "New Service Request Added";
                    //E-mail message body
                    message.Body = new TextPart(TextFormat.Html)
                    {
                        Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                       "<br/><br/><br/><br/>" +
                    "<p>Dear Colleague,</p>" +

                   "<p> This is to notify you that a new leave request has been sent by " + user.FirstName + " " + user.LastName +

                   "<br/>on </a>" + leaves.CreateDate + ".</p>" +
                   "Unit Head can see more information on the leave request by Clicking <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +




                                                  "<br/>Regards<br/>" +


                                                   "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                    };

                    //Configure the e-mail
                    using (var emailClient = new SmtpClient())
                    {

                        emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                        emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                        //emailClient.Connect("smtp.office365.com", 587, false);
                        //emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                        //emailClient.Send(message);
                        //emailClient.Disconnect(true);
                    }


                
            }
            else
            {
                if (id != leaves.Id)
                {
                    return NotFound();
                }

               
                    try
                    {
                        _context.Update(leaves);
                        await _context.SaveChangesAsync();
                        if (leaves.isUnitHeadApproved == true && leaves.isHrApproved == true)
                        {
                            var user = await _userManager.GetUserAsync(User);
                            ViewBag.ToEmail = user.Email;
                            var fromEmail = user.Email;
                            var fromName = user.FirstName + ' ' + user.LastName;

                            var eee = (from a in _context.Users
                                       join b in _context.UserRoles on a.Id equals b.UserId
                                       join c in _context.Roles on b.RoleId equals c.Id
                                       where a.Id == b.UserId && c.Name == "UnitHead" && a.UnitsId == user.UnitsId
                                       select new
                                       {
                                           a.Email,
                                           a.FirstName,
                                           a.LastName

                                       }).FirstOrDefault();

                            ViewBag.FromEmail = eee.Email.ToString();

                            //var use = Int32.Parse(_context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeId).LastOrDefault().ToString());


                            //var uuuu = _context.Users.Find(use);

                            //var ttyt = uuuu.Email.ToString();
                            //var tttt = uuuu.FirstName.ToString() + ' ' + uuuu.LastName.ToString(); 
                            var use = _context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeEmail).FirstOrDefault();


                            //var tttt = _context.Users.Find(use).Email.ToString();


                            var toEmail = eee.Email.ToString();
                            var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();

                            //var ffff = _context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeId);

                            //instantiate a new MimeMessage
                            var message = new MimeMessage();
                            //Setting the To e-mail address
                            message.To.Add(new MailboxAddress(use));

                            //message.Bcc.Add(new MailboxAddress(CompanyEmail));
                            var tt = "akinbamidelea@novohealthafrica.org";
                            message.Bcc.Add(new MailboxAddress(tt));
                            //Setting the From e-mail address
                            message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                            //E-mail subject 
                            message.Subject = "New Service Request Added";
                            //E-mail message body
                            message.Body = new TextPart(TextFormat.Html)
                            {

                                Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                               "<br/><br/><br/><br/>" +
                            "<p>Dear</p>" + leaves.AddedBy + "," +

                           "<p> This is to notify you that the leave request you sent on " + leaves.CreateDate + "has been approved by your Unit Head and the HR." +
                           "For more inforation on the leave request, Click <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +



                                                          "<br/>Regards<br/>" +


                                                           "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"

                            };

                            //Configure the e-mail
                            using (var emailClient = new SmtpClient())
                            {

                                emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                                emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                                emailClient.Connect("smtp.office365.com", 587, false);
                                emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                                emailClient.Send(message);
                                emailClient.Disconnect(true);
                            }
                        }
                        else if (leaves.isUnitHeadApproved == true && leaves.isHrApproved == false)
                        {
                            var user = await _userManager.GetUserAsync(User);
                            ViewBag.ToEmail = user.Email;
                            var fromEmail = user.Email;
                            var fromName = user.FirstName + ' ' + user.LastName;

                            var eee = (from a in _context.Users
                                       join b in _context.UserRoles on a.Id equals b.UserId
                                       join c in _context.Roles on b.RoleId equals c.Id
                                       where a.Id == b.UserId && c.Name == "UnitHead" && a.UnitsId == user.UnitsId
                                       select new
                                       {
                                           a.Email,
                                           a.FirstName,
                                           a.LastName

                                       }).FirstOrDefault();

                            ViewBag.FromEmail = eee.Email.ToString();

                            var use = _context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeEmail).FirstOrDefault();

                            var toEmail = eee.Email.ToString();
                            var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();

                            //instantiate a new MimeMessage
                            var message = new MimeMessage();
                            //Setting the To e-mail address
                            message.To.Add(new MailboxAddress(use));

                            var tt = "akinbamidelea@novohealthafrica.org";
                            message.Bcc.Add(new MailboxAddress(tt));
                            //Setting the From e-mail address
                            message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                            //E-mail subject 
                            message.Subject = "New Service Request Added";
                            //E-mail message body
                            message.Body = new TextPart(TextFormat.Html)
                            {
                                Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                               "<br/><br/><br/><br/>" +
                            "<p>Dear</p>" + leaves.AddedBy + "," +

                           "<p> This is to notify you that the leave request you sent on " + leaves.CreateDate + "has been approved by your Unit Head but rejected by the Head of Human Resource Unit." +
                          "For more inforation on the leave request, Click <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +




                                                          "<br/>Regards<br/>" +


                                                           "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"

                            };

                            //Configure the e-mail
                            using (var emailClient = new SmtpClient())
                            {

                                emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                                emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                                emailClient.Connect("smtp.office365.com", 587, false);
                                emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                                emailClient.Send(message);
                                emailClient.Disconnect(true);
                            }
                        }
                        else if (leaves.isUnitHeadApproved == true)
                        {
                            var user = await _userManager.GetUserAsync(User);
                            ViewBag.ToEmail = user.Email;
                            var fromEmail = user.Email;
                            var fromName = user.FirstName + ' ' + user.LastName;

                            var eee = (from a in _context.Users
                                       join b in _context.UserRoles on a.Id equals b.UserId
                                       join c in _context.Roles on b.RoleId equals c.Id
                                       where a.Id == b.UserId && c.Name == "HR"
                                       select new
                                       {
                                           a.Email,
                                           a.FirstName,
                                           a.LastName

                                       }).FirstOrDefault();

                            ViewBag.FromEmail = eee.Email.ToString();
                            var toEmail = eee.Email.ToString();
                            var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();


                            //instantiate a new MimeMessage
                            var message = new MimeMessage();
                            //Setting the To e-mail address
                            message.To.Add(new MailboxAddress(toName, toEmail));

                            //message.Bcc.Add(new MailboxAddress(CompanyEmail));
                            var tt = "akinbamidelea@novohealthafrica.org";
                            message.Bcc.Add(new MailboxAddress(tt));
                            //Setting the From e-mail address
                            message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                            //E-mail subject 
                            message.Subject = "New Leave Request";
                            //E-mail message body
                            message.Body = new TextPart(TextFormat.Html)
                            {
                                Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                               "<br/><br/><br/><br/>" +
                            "<p>Dear</p>" + leaves.AddedBy + "," +

                           "<p> This is to notify you that the leave request you sent on " + leaves.CreateDate + "has been approved by your Unit Head but awaiting approval by the Head of Human Resource Unit." +
                           "For more inforation on the leave request, Click <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +





                                                          "<br/>Regards<br/>" +


                                                           "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"

                            };

                            //Configure the e-mail
                            using (var emailClient = new SmtpClient())
                            {

                                emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                                emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                                emailClient.Connect("smtp.office365.com", 587, false);
                                emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                                emailClient.Send(message);
                                emailClient.Disconnect(true);
                            }
                        }
                        else if (leaves.isUnitHeadApproved == false)
                        {
                            var user = await _userManager.GetUserAsync(User);
                            ViewBag.ToEmail = user.Email;
                            var fromEmail = user.Email;
                            var fromName = user.FirstName + ' ' + user.LastName;

                            var eee = (from a in _context.Users
                                       join b in _context.UserRoles on a.Id equals b.UserId
                                       join c in _context.Roles on b.RoleId equals c.Id
                                       where a.Id == b.UserId && c.Name == "UnitHead" && a.UnitsId == user.UnitsId
                                       select new
                                       {
                                           a.Email,
                                           a.FirstName,
                                           a.LastName
                                       }).FirstOrDefault();

                            ViewBag.FromEmail = eee.Email.ToString();

                            var use = _context.Leaves.Where(x => x.Id == id).Select(x => x.EmployeeEmail).FirstOrDefault();

                            var toEmail = eee.Email.ToString();
                            var toName = eee.FirstName.ToString() + ' ' + eee.LastName.ToString();

                            //instantiate a new MimeMessage
                            var message = new MimeMessage();
                            //Setting the To e-mail address
                            message.To.Add(new MailboxAddress(use));

                            var tt = "akinbamidelea@novohealthafrica.org";
                            message.Bcc.Add(new MailboxAddress(tt));
                            //Setting the From e-mail address
                            message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                            //E-mail subject 
                            message.Subject = "New Service Request Added";
                            //E-mail message body
                            message.Body = new TextPart(TextFormat.Html)
                            {
                                Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                               "<br/><br/><br/><br/>" +
                            "<p>Dear</p>" + leaves.AddedBy + "," +

                           "<p> This is to notify you that the leave request you sent on " + leaves.CreateDate + "has been rejected by your Unit Head." +
                          "For more inforation on the leave request, Click <a href='https://hrms.novohealthafrica.org/leaves/Details/" + leaves.Id + "'>Here</a>" +



                                                          "<br/>Regards<br/>" +


                                                           "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"

                            };

                            //Configure the e-mail
                            using (var emailClient = new SmtpClient())
                            {
                                emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                                emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                                emailClient.Connect("smtp.office365.com", 587, false);
                                emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2007");
                                emailClient.Send(message);
                                emailClient.Disconnect(true);
                            }
                        }
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!LeavesExists(leaves.Id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }


                    if (_signInManager.IsSignedIn(User) && User.IsInRole("UnitHead"))
                    {
                        return RedirectToAction("leaveslist", "leaves");
                    }
                    else if (_signInManager.IsSignedIn(User) && User.IsInRole("HR"))
                    {
                        return RedirectToAction("leaves", "leaves");
                    }
                // return RedirectToAction(nameof(Index));
                return Json(new { isValid = true, html = Helper.RenderRazorViewToString(this, "Index", _context.Leaves.ToList()) });
            }


            //ViewData["LeaveTypeId"] = new SelectList(_context.LeaveTypes, "Id", "Type", leaves.LeaveTypeId);
            //ViewData["UnitsId"] = new SelectList(_context.Units, "Id", "Name", leaves.UnitsId);
            //return View(leaves);
            //return Json(new { isValid = true, html = Helper.RenderRazorViewToString(this, "Index", _context.Leaves.ToList()) });
            return RedirectToAction(nameof(Index));
        }



        // GET: Leaves/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var leaves = await _context.Leaves
                .Include(l => l.LeaveType)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (leaves == null)
            {
                return NotFound();
            }

            return View(leaves);
        }

        // POST: Leaves/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var leaves = await _context.Leaves.FindAsync(id);
            _context.Leaves.Remove(leaves);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool LeavesExists(int id)
        {
            return _context.Leaves.Any(e => e.Id == id);
        }
              
    }
}
