﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;
using NovoApp.Moddel;
using NovoClients.DataAccess.Data;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace NovoApp.Controllers
{
    public class AppraisalController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public AppraisalController(ApplicationDbContext context,
                                    SignInManager<ApplicationUser> signInManager,
                                    UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        // GET: Appraisal
        public async Task<IActionResult> Index()
        {
            return View(await _context.Appraisal.ToListAsync());
        }

        [Authorize(Roles = "UnitHead, DeptHead, Admin, SuperAdmin")]
        public async Task<IActionResult> AppraisalList()
        {

            var user = await _userManager.GetUserAsync(User);

            var AppraisalList = _context.Appraisal.Where(x => x.UnitsId == user.UnitsId).Where(x => x.EmployeeId != user.Id).ToList();

            return View(AppraisalList);
        }

        [Authorize(Roles = "HR")]
        public async Task<IActionResult> AppraisalLists()
        {

            var user = await _userManager.GetUserAsync(User);

            var AppraisalList = _context.Appraisal.Where(x => x.IsDelete == false && User.IsInRole("UnitHead")).ToList();

            return View(AppraisalList);
        }


        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appraisalForm = await _context.Appraisal.FindAsync(id);
            FormsViewModel formsViewModel = new FormsViewModel();
            formsViewModel.appraisalForm = appraisalForm;
            formsViewModel.form = _context.Forms.Where(x => x.EmployeeId == appraisalForm.EmployeeId).FirstOrDefault();


            if (formsViewModel == null)
            {
                return NotFound();
            }

            return View(formsViewModel);
        }

        [HttpGet]
        // GET: AppraisalForms/Create
        public async Task<IActionResult> Createe()
        {
            var user = await _userManager.GetUserAsync(User);
            ViewBag.Id = _context.Forms.Where(a => a.EmployeeId == user.Id).Select(x => x.EmployeeId).FirstOrDefault();
            FormsViewModel formsViewModel = new FormsViewModel();
            formsViewModel.appraisalForm = _context.Appraisal.FirstOrDefault();
            formsViewModel.form = _context.Forms.Where(x => x.EmployeeId == user.Id).FirstOrDefault();
            return View(formsViewModel);
        }

        [HttpGet]
        // GET: AppraisalForms/Create
        public async Task<IActionResult> Create()
        {
            var user = await _userManager.GetUserAsync(User);
            ViewBag.Id = _context.Forms.Where(a => a.EmployeeId == user.Id).Select(x => x.EmployeeId).FirstOrDefault();
            FormsViewModel formsViewModel = new FormsViewModel();
            formsViewModel.appraisalForm = _context.Appraisal.FirstOrDefault();
            formsViewModel.form = _context.Forms.Where(x => x.EmployeeId == user.Id).FirstOrDefault();
            if(formsViewModel.form == null)
            {
                return NotFound();
            }
            return View(formsViewModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(FormsViewModel model)
        {
            if (ModelState.IsValid)
            {
                _context.Appraisal.Add(model.appraisalForm);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
          
            return View();
        }

        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appraisalForm = await _context.Appraisal.FindAsync(id);
            FormsViewModel formsViewModel = new FormsViewModel();
            formsViewModel.appraisalForm = appraisalForm;
            formsViewModel.form = _context.Forms.Where(x => x.EmployeeId == appraisalForm.EmployeeId).FirstOrDefault();

            if (formsViewModel == null)
            {
                return NotFound();
            }
            return View(formsViewModel);
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(FormsViewModel model)
        {
            var employeeFromDb = _context.Appraisal.Find(model.appraisalForm.Id);
            if (model.appraisalForm == null)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    employeeFromDb.OperationalExpensesReductionAppraisal = model.appraisalForm.OperationalExpensesReductionAppraisal;
                    employeeFromDb.MedicalBillsReductionAppraisal = model.appraisalForm.MedicalBillsReductionAppraisal;
                    employeeFromDb.RetailProductAppraisal = model.appraisalForm.RetailProductAppraisal;
                    employeeFromDb.TurnAroundTimeAppraisal = model.appraisalForm.TurnAroundTimeAppraisal;
                    employeeFromDb.MinimalComplaintsAppraisal = model.appraisalForm.MinimalComplaintsAppraisal;
                    employeeFromDb.ConstantCommunicationAppraisal = model.appraisalForm.ConstantCommunicationAppraisal;
                    employeeFromDb.TimelyCompletionAppraisal = model.appraisalForm.TimelyCompletionAppraisal;
                    employeeFromDb.TimelyAuditAppraisal = model.appraisalForm.TimelyAuditAppraisal;
                    employeeFromDb.ZeroNonComplianceAppraisal = model.appraisalForm.ZeroNonComplianceAppraisal;
                    employeeFromDb.PromptReviewAppraisal = model.appraisalForm.PromptReviewAppraisal;
                    employeeFromDb.ZeroProcessFailuresAppraisal = model.appraisalForm.ZeroProcessFailuresAppraisal;
                    employeeFromDb.InternalProcessAppraisal = model.appraisalForm.InternalProcessAppraisal;
                    employeeFromDb.AdhocTasksAppraisal = model.appraisalForm.AdhocTasksAppraisal;
                    employeeFromDb.TrainingHoursAppraisal = model.appraisalForm.TrainingHoursAppraisal;
                    employeeFromDb.DisciplinaryRecordAppraisal = model.appraisalForm.DisciplinaryRecordAppraisal;
                    employeeFromDb.CultureAppraisal = model.appraisalForm.CultureAppraisal;
                    employeeFromDb.AttendanceAndPunctualityAppraisal = model.appraisalForm.AttendanceAndPunctualityAppraisal;
                    employeeFromDb.TechnologyProficiencyAppraisal = model.appraisalForm.TechnologyProficiencyAppraisal;
                    employeeFromDb.TeamworkAppraisal = model.appraisalForm.TeamworkAppraisal;
                    employeeFromDb.ProfessionalismAppraisal = model.appraisalForm.ProfessionalismAppraisal;
                    employeeFromDb.CostConciousnessAppraisal = model.appraisalForm.CostConciousnessAppraisal;
                    employeeFromDb.CommunicationAppraisal = model.appraisalForm.CommunicationAppraisal;
                    employeeFromDb.PersonalDisciplineAppraisal = model.appraisalForm.PersonalDisciplineAppraisal;
                    employeeFromDb.CustomerServiceAppraisal = model.appraisalForm.CustomerServiceAppraisal;
                    employeeFromDb.InnovationAppraisal = model.appraisalForm.InnovationAppraisal;
                    employeeFromDb.ManagementSkillsAppraisal = model.appraisalForm.ManagementSkillsAppraisal;
                    employeeFromDb.ContinuousImprovementAppraisal = model.appraisalForm.ContinuousImprovementAppraisal;
                    employeeFromDb.MeetingDeadlinesAppraisal = model.appraisalForm.MeetingDeadlinesAppraisal;
                    employeeFromDb.Question7Appraisail = model.appraisalForm.Question7Appraisail;
                    employeeFromDb.Question8Appraisal = model.appraisalForm.Question8Appraisal;

                    employeeFromDb.FinancialSubTotalAppraisal = model.appraisalForm.OperationalExpensesReductionAppraisal + model.appraisalForm.MedicalBillsReductionAppraisal + model.appraisalForm.RetailProductAppraisal;

                    employeeFromDb.CustomerSatisfactionTotalAppraisal = model.appraisalForm.TurnAroundTimeAppraisal + model.appraisalForm.MinimalComplaintsAppraisal + model.appraisalForm.ConstantCommunicationAppraisal + 
                                                                        model.appraisalForm.Question7Appraisail + model.appraisalForm.Question8Appraisal;



                    employeeFromDb.ProcessTotalAppraisal = model.appraisalForm.TimelyCompletionAppraisal + model.appraisalForm.TimelyAuditAppraisal + model.appraisalForm.ZeroNonComplianceAppraisal +
                        model.appraisalForm.PromptReviewAppraisal + model.appraisalForm.ZeroProcessFailuresAppraisal + model.appraisalForm.InternalProcessAppraisal +
                        model.appraisalForm.AdhocTasksAppraisal;

                    employeeFromDb.LearningTotalAppraisal = model.appraisalForm.TrainingHoursAppraisal + model.appraisalForm.DisciplinaryRecordAppraisal + model.appraisalForm.AttendanceAndPunctualityAppraisal +
                        model.appraisalForm.CultureAppraisal + model.appraisalForm.TechnologyProficiencyAppraisal;


                    employeeFromDb.SectionAScoreAppraisal = model.appraisalForm.OperationalExpensesReductionAppraisal +
                        model.appraisalForm.MedicalBillsReductionAppraisal + model.appraisalForm.RetailProductAppraisal + model.appraisalForm.TurnAroundTimeAppraisal +
                        model.appraisalForm.MinimalComplaintsAppraisal + model.appraisalForm.ConstantCommunicationAppraisal + model.appraisalForm.TimelyCompletionAppraisal +
                        model.appraisalForm.TimelyAuditAppraisal + model.appraisalForm.ZeroNonComplianceAppraisal + model.appraisalForm.ZeroProcessFailuresAppraisal +
                        model.appraisalForm.InternalProcessAppraisal + model.appraisalForm.AdhocTasksAppraisal + model.appraisalForm.TrainingHoursAppraisal +
                        model.appraisalForm.DisciplinaryRecordAppraisal + model.appraisalForm.CultureAppraisal + model.appraisalForm.AttendanceAndPunctualityAppraisal +
                        model.appraisalForm.TechnologyProficiencyAppraisal + model.appraisalForm.PromptReviewAppraisal;

                    employeeFromDb.AppraiserBTotalScore = model.appraisalForm.TeamworkAppraisal + model.appraisalForm.ProfessionalismAppraisal + model.appraisalForm.CostConciousnessAppraisal +
                        model.appraisalForm.CommunicationAppraisal + model.appraisalForm.PersonalDisciplineAppraisal +
                        model.appraisalForm.CustomerServiceAppraisal + model.appraisalForm.InnovationAppraisal + model.appraisalForm.ManagementSkillsAppraisal +
                        model.appraisalForm.ContinuousImprovementAppraisal + model.appraisalForm.MeetingDeadlinesAppraisal;

                    Console.WriteLine(employeeFromDb.AppraiserBTotalScore);

                    employeeFromDb.SectionBTotal = (employeeFromDb.AppraiserBTotalScore * 25) / 30;

                    employeeFromDb.FinalScore = ((employeeFromDb.SectionBTotal * 4) + (employeeFromDb.SectionAScoreAppraisal * 1.33)) / 2;

                    //appraisalForm.FinalScore = Convert.ToInt32(employeeFromDb.FinalScore);
                    //var x = employeeFromDb.FinalScore;

                    if (Convert.ToInt32(employeeFromDb.FinalScore) >= 85)
                    {
                        employeeFromDb.Grade = "A";
                    }
                    else if (61 <= Convert.ToInt32(employeeFromDb.FinalScore))
                    {
                        employeeFromDb.Grade = "B";
                    }
                    else if (51 <= Convert.ToInt32(employeeFromDb.FinalScore))
                    {
                        employeeFromDb.Grade = "C";
                    }
                    else
                    {
                        employeeFromDb.Grade = "D";
                    }

                    //_context.Update(appraisalForm);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AppraisalExists(model.appraisalForm.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(AppraisalList));
            }
            return View(model.appraisalForm);
        }





        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("EmployeeId,EmployeeEmail,OperationalExpensesReduction,OperationalExpensesReductionAppraisal,MedicalBillsReduction,MedicalBillsReductionAppraisal,RetailProduct,RetailProductAppraisal,FinancialSubTotal,FinancialSubTotalAppraisal,TurnAroundTime,TurnAroundTimeAppraisal,MinimalComplaints,MinimalComplaintsAppraisal,ConstantCommunication,ConstantCommunicationAppraisal,CustomerSatisfactionTotal,CustomerSatisfactionTotalAppraisal,TimelyCompletion,TimelyCompletionAppraisal,TimelyAudit,TimelyAuditAppraisal,ZeroNonCompliance,ZeroNonComplianceAppraisal,PromptReview,PromptReviewAppraisal,ZeroProcessFailures,ZeroProcessFailuresAppraisal,InternalProcess,InternalProcessAppraisal,AdhocTasks,AdhocTasksAppraisal,ProcessTotal,ProcessTotalAppraisal,TrainingHours,TrainingHoursAppraisal,DisciplinaryRecord,DisciplinaryRecordAppraisal,Culture,CultureAppraisal,AttendanceAndPunctuality,AttendanceAndPunctualityAppraisal,TechnologyProficiency,TechnologyProficiencyAppraisal,LearningTotal,LearningTotalAppraisal,Teamwork,TeamworkAppraisal,Professionalism,ProfessionalismAppraisal,CostConciousness,CostConciousnessAppraisal,Communication,CommunicationAppraisal,PersonalDiscipline,PersonalDisciplineAppraisal,CustomerService,CustomerServiceAppraisal,Innovation,InnovationAppraisal,ManagementSkills,ManagementSkillsAppraisal,ContinuousImprovement,ContinuousImprovementAppraisal,MeetingDeadlines,MeetingDeadlinesAppraisal,EmployeeBTotalScore,AppraiserBTotalScore,ExceptionalAchievements,SectionAScore,SectionAScoreAppraisal,SectionBTotal,FinalScore,Grade,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] AppraisalForm appraisalForm)
        //{
        //    var employeeFromDb = _context.Appraisal.Find(appraisalForm.Id);
        //    if (id != appraisalForm.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            employeeFromDb.OperationalExpensesReductionAppraisal = appraisalForm.OperationalExpensesReductionAppraisal;
        //            employeeFromDb.MedicalBillsReductionAppraisal = appraisalForm.MedicalBillsReductionAppraisal;
        //            employeeFromDb.RetailProductAppraisal = appraisalForm.RetailProductAppraisal;
        //            employeeFromDb.TurnAroundTimeAppraisal = appraisalForm.TurnAroundTimeAppraisal;
        //            employeeFromDb.MinimalComplaintsAppraisal = appraisalForm.MinimalComplaintsAppraisal;
        //            employeeFromDb.ConstantCommunicationAppraisal = appraisalForm.ConstantCommunicationAppraisal;
        //            employeeFromDb.TimelyCompletionAppraisal = appraisalForm.TimelyCompletionAppraisal;
        //            employeeFromDb.TimelyAuditAppraisal = appraisalForm.TimelyAuditAppraisal;
        //            employeeFromDb.ZeroNonComplianceAppraisal = appraisalForm.ZeroNonComplianceAppraisal;
        //            employeeFromDb.PromptReviewAppraisal = appraisalForm.PromptReviewAppraisal;
        //            employeeFromDb.ZeroProcessFailuresAppraisal = appraisalForm.ZeroProcessFailuresAppraisal;
        //            employeeFromDb.InternalProcessAppraisal = appraisalForm.InternalProcessAppraisal;
        //            employeeFromDb.AdhocTasksAppraisal = appraisalForm.AdhocTasksAppraisal;
        //            employeeFromDb.TrainingHoursAppraisal = appraisalForm.TrainingHoursAppraisal;
        //            employeeFromDb.DisciplinaryRecordAppraisal = appraisalForm.DisciplinaryRecordAppraisal;
        //            employeeFromDb.CultureAppraisal = appraisalForm.CultureAppraisal;
        //            employeeFromDb.AttendanceAndPunctualityAppraisal = appraisalForm.AttendanceAndPunctualityAppraisal;
        //            employeeFromDb.TechnologyProficiencyAppraisal = appraisalForm.TechnologyProficiencyAppraisal;
        //            employeeFromDb.TeamworkAppraisal = appraisalForm.TeamworkAppraisal;
        //            employeeFromDb.ProfessionalismAppraisal = appraisalForm.ProfessionalismAppraisal;
        //            employeeFromDb.CostConciousnessAppraisal = appraisalForm.CostConciousnessAppraisal;
        //            employeeFromDb.CommunicationAppraisal = appraisalForm.CommunicationAppraisal;
        //            employeeFromDb.PersonalDisciplineAppraisal = appraisalForm.PersonalDisciplineAppraisal;
        //            employeeFromDb.CustomerServiceAppraisal = appraisalForm.CustomerServiceAppraisal;
        //            employeeFromDb.InnovationAppraisal = appraisalForm.InnovationAppraisal;
        //            employeeFromDb.ManagementSkillsAppraisal = appraisalForm.ManagementSkillsAppraisal;
        //            employeeFromDb.ContinuousImprovementAppraisal = appraisalForm.ContinuousImprovementAppraisal;
        //            employeeFromDb.MeetingDeadlinesAppraisal = appraisalForm.MeetingDeadlinesAppraisal;

        //            employeeFromDb.FinancialSubTotalAppraisal = appraisalForm.OperationalExpensesReductionAppraisal + appraisalForm.MedicalBillsReductionAppraisal + appraisalForm.RetailProductAppraisal;

        //            employeeFromDb.CustomerSatisfactionTotalAppraisal = appraisalForm.TurnAroundTimeAppraisal + appraisalForm.MinimalComplaintsAppraisal + appraisalForm.ConstantCommunicationAppraisal;

        //            employeeFromDb.ProcessTotalAppraisal = appraisalForm.TimelyCompletionAppraisal + appraisalForm.TimelyAuditAppraisal + appraisalForm.ZeroNonComplianceAppraisal +
        //                appraisalForm.PromptReviewAppraisal + appraisalForm.ZeroProcessFailuresAppraisal + appraisalForm.InternalProcessAppraisal +
        //                appraisalForm.AdhocTasksAppraisal;

        //            employeeFromDb.LearningTotalAppraisal = appraisalForm.TrainingHoursAppraisal + appraisalForm.DisciplinaryRecordAppraisal + appraisalForm.AttendanceAndPunctualityAppraisal +
        //                appraisalForm.CultureAppraisal + appraisalForm.TechnologyProficiencyAppraisal;


        //            employeeFromDb.SectionAScoreAppraisal = appraisalForm.OperationalExpensesReductionAppraisal +
        //                appraisalForm.MedicalBillsReductionAppraisal + appraisalForm.RetailProductAppraisal + appraisalForm.TurnAroundTimeAppraisal +
        //                appraisalForm.MinimalComplaintsAppraisal + appraisalForm.ConstantCommunicationAppraisal + appraisalForm.TimelyCompletionAppraisal +
        //                appraisalForm.TimelyAuditAppraisal + appraisalForm.ZeroNonComplianceAppraisal + appraisalForm.ZeroProcessFailuresAppraisal +
        //                appraisalForm.InternalProcessAppraisal + appraisalForm.AdhocTasksAppraisal + appraisalForm.TrainingHoursAppraisal +
        //                appraisalForm.DisciplinaryRecordAppraisal + appraisalForm.CultureAppraisal + appraisalForm.AttendanceAndPunctualityAppraisal +
        //                appraisalForm.TechnologyProficiencyAppraisal + appraisalForm.PromptReviewAppraisal;

        //            employeeFromDb.AppraiserBTotalScore = appraisalForm.TeamworkAppraisal + appraisalForm.ProfessionalismAppraisal + appraisalForm.CostConciousnessAppraisal +
        //                appraisalForm.CommunicationAppraisal + appraisalForm.PersonalDisciplineAppraisal +
        //                appraisalForm.CustomerServiceAppraisal + appraisalForm.InnovationAppraisal + appraisalForm.ManagementSkillsAppraisal +
        //                appraisalForm.ContinuousImprovementAppraisal + appraisalForm.MeetingDeadlinesAppraisal;

        //            Console.WriteLine(employeeFromDb.AppraiserBTotalScore);

        //            employeeFromDb.SectionBTotal = (employeeFromDb.AppraiserBTotalScore * 25) / 30;

        //            employeeFromDb.FinalScore = ((employeeFromDb.SectionBTotal * 4) + (employeeFromDb.SectionAScoreAppraisal * 1.33)) / 2;

        //            //appraisalForm.FinalScore = Convert.ToInt32(employeeFromDb.FinalScore);
        //            //var x = employeeFromDb.FinalScore;

        //            if (Convert.ToInt32(employeeFromDb.FinalScore) >= 85)
        //            {
        //                employeeFromDb.Grade = "A";
        //            }
        //            else if (61 <= Convert.ToInt32(employeeFromDb.FinalScore))
        //            {
        //                employeeFromDb.Grade = "B";
        //            }
        //            else if (51 <= Convert.ToInt32(employeeFromDb.FinalScore))
        //            {
        //                employeeFromDb.Grade = "C";
        //            }
        //            else
        //            {
        //                employeeFromDb.Grade = "D";
        //            }

        //            //_context.Update(appraisalForm);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!AppraisalExists(appraisalForm.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(AppraisalList));
        //    }
        //    return View(appraisalForm);
        //}

        private bool AppraisalExists(int id)
        {
            return _context.Appraisal.Any(e => e.Id == id);
        }
    }
}
