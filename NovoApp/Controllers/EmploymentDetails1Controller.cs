﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;
using Novo.Models.ViewModels;
using NovoClients.DataAccess.Data;

namespace NovoApp.Controllers
{
    public class EmploymentDetails1Controller : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public EmploymentDetails1Controller(ApplicationDbContext context,
                                                SignInManager<ApplicationUser> signInManager,
                                                UserManager<ApplicationUser> userManager,
                                                IHostingEnvironment hostingEnvironment)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        // GET: EmploymentDetails1
        public async Task<IActionResult> Display()
        {
            var employmentDetails = await _context.EmploymentDetails.ToListAsync();
            return new JsonResult(employmentDetails);
        }


        public IActionResult Index()
        {
            return View();
        }

        // GET: EmploymentDetails1/Details/5
        public async Task<IActionResult> Details()
        {
            var user = await _userManager.GetUserAsync(User);
            //ViewBag.Unit = _context.Units.Where(x => x.Id == user.Id).Select(x => x.Name);


            ViewBag.Unit = _context.Units.Where(x => x.Id == user.UnitsId).Select(x => x.Name).FirstOrDefault();

            ViewBag.Leaves = _context.Leaves.Where(x => x.EmployeeId == user.Id).Select(x => x.TotalLeave).LastOrDefault();
            ViewBag.Utilized = _context.Leaves.Where(x => x.EmployeeEmail == user.Email).Count();
            ViewBag.Remaining = ViewBag.Leaves - ViewBag.Utilized;
            //ViewBag.Remaining = _context.Leaves.Where(x => x.EmployeeId == user.Id).Select(x => x.LeaveRemaining).LastOrDefault();

            var employeedetails = _context.EmploymentDetails.Where(x => x.EmployeeId == user.Id)
                // .Include(e => e.Units)
                .FirstOrDefault();

            if (employeedetails == null)
            {
                return RedirectToAction("Create", "EmploymentDetails1");
            }

            return View(employeedetails);
        }

        public async Task<IActionResult> Detail(int? id)
        {
            //var user = await _userManager.FindByIdAsync(id.ToString());
            //ViewBag.Unit = _context.Units.Where(x => x.Id == user.Id).Select(x => x.Name);


            //ViewBag.Unit = _context.Units.Where(x => x.Id == user.UnitsId).Select(x => x.Name).FirstOrDefault();

            //ViewBag.Leaves = _context.Leaves.Where(x => x.EmployeeId == user.Id).Select(x => x.TotalLeave).LastOrDefault();
            //ViewBag.Utilized = _context.Leaves.Where(x => x.EmployeeEmail == user.Email).Count();
            //ViewBag.Remaining = ViewBag.Leaves - ViewBag.Utilized;
            //ViewBag.Remaining = _context.Leaves.Where(x => x.EmployeeId == user.Id).Select(x => x.LeaveRemaining).LastOrDefault();

            //var employeedetails = _context.EmploymentDetails.Where(x => x.EmployeeId == user.Id)
            //    // .Include(e => e.Units)
            //    .FirstOrDefault();

            //if (employeedetails == null)
            //{
            //    return RedirectToAction("Create", "EmploymentDetails1");
            //}
            if (id == null)
            {
                return NotFound();
            }

            var employeedetails = await _context.EmploymentDetails
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employeedetails == null)
            {
                return NotFound();
            }

           

            return View(employeedetails);
        }

        // GET: EmploymentDetails1/Create
        public IActionResult Create()
        {
            var model = new EmployeeVm();
            return View(model);
        }

        // POST: EmploymentDetails1/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EmployeeVm employmentDetails)
        {
            if (ModelState.IsValid)
            {
                string uniqueFileName = null;
               
                if (employmentDetails.Image != null && employmentDetails.Image.Length > 0)
                {

                   string uploadFolder = Path.Combine(_hostingEnvironment.WebRootPath, "images/Employees");
                   uniqueFileName = Guid.NewGuid().ToString() + "_" + employmentDetails.Image.FileName;
                   string filePath = Path.Combine(uploadFolder, uniqueFileName);
                   employmentDetails.Image.CopyTo(new FileStream(filePath, FileMode.Create));

                }

                var model = new EmploymentDetails
                {
                    EmployeeId = employmentDetails.EmployeeId,
                    Fullname = employmentDetails.Fullname,
                    BloodGroup = employmentDetails.BloodGroup,
                    DOB = employmentDetails.DOB,
                    Gender = employmentDetails.Gender,
                    MaritalStatus = employmentDetails.MaritalStatus,
                    Designation = employmentDetails.Designation,
                    EmployeeNo = employmentDetails.EmployeeNo,
                    Email = employmentDetails.Email,
                    ModeOfEmployment = employmentDetails.ModeOfEmployment,
                    JobTitle = employmentDetails.JobTitle,
                    EntryLevel = employmentDetails.EntryLevel,
                    IsUnitHead = employmentDetails.IsUnitHead,
                    IsDeptHead = employmentDetails.IsDeptHead,
                    IsRegionalHead = employmentDetails.IsRegionalHead,
                    CurrentLevel = employmentDetails.CurrentLevel,
                    LastPromotion = employmentDetails.LastPromotion,
                    DateOfEmployment = employmentDetails.DateOfEmployment,
                    DateOfLeaving = employmentDetails.DateOfLeaving,
                    YearsOfExperience = employmentDetails.YearsOfExperience,
                    CUG = employmentDetails.CUG,
                    Image = uniqueFileName
                };
                _context.EmploymentDetails.Add(model);
                await _context.SaveChangesAsync();
               return RedirectToAction("Create","EmployeeContacts");
            }
            return View(employmentDetails);
        }

        // GET: EmploymentDetails1/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employmentDetails = await _context.EmploymentDetails.FindAsync(id);
            if (employmentDetails == null)
            {
                return NotFound();
            }

            var model = new EmployeeEditVm()
            {
                EmployeeId=employmentDetails.EmployeeId,
                Fullname=employmentDetails.Fullname,
                BloodGroup=employmentDetails.BloodGroup,
                DOB=employmentDetails.DOB,
                Gender=employmentDetails.Gender,
                MaritalStatus=employmentDetails.MaritalStatus,
                Designation=employmentDetails.Designation,
                EmployeeNo=employmentDetails.EmployeeNo,
                Email=employmentDetails.Email,
                ModeOfEmployment=employmentDetails.ModeOfEmployment,
                JobTitle=employmentDetails.JobTitle,
                EntryLevel=employmentDetails.EntryLevel,
                IsUnitHead=employmentDetails.IsUnitHead,
                IsDeptHead=employmentDetails.IsDeptHead,
                IsRegionalHead=employmentDetails.IsRegionalHead,
                CurrentLevel=employmentDetails.CurrentLevel,
                LastPromotion=employmentDetails.LastPromotion,
                DateOfEmployment=employmentDetails.DateOfEmployment,
                DateOfLeaving=employmentDetails.DateOfLeaving,
                YearsOfExperience=employmentDetails.YearsOfExperience,
                CUG=employmentDetails.CUG,


            };
            return View(model);
        }

        public async Task<IActionResult> Edits(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employmentDetails = await _context.EmploymentDetails.FindAsync(id);
            if (employmentDetails == null)
            {
                return NotFound();
            }

            var model = new EmployeeEditVm()
            {
                EmployeeId = employmentDetails.EmployeeId,
                Fullname = employmentDetails.Fullname,
                BloodGroup = employmentDetails.BloodGroup,
                DOB = employmentDetails.DOB,
                Gender = employmentDetails.Gender,
                MaritalStatus = employmentDetails.MaritalStatus,
                Designation = employmentDetails.Designation,
                EmployeeNo = employmentDetails.EmployeeNo,
                Email = employmentDetails.Email,
                ModeOfEmployment = employmentDetails.ModeOfEmployment,
                JobTitle = employmentDetails.JobTitle,
                EntryLevel = employmentDetails.EntryLevel,
                IsUnitHead = employmentDetails.IsUnitHead,
                IsDeptHead = employmentDetails.IsDeptHead,
                IsRegionalHead = employmentDetails.IsRegionalHead,
                CurrentLevel = employmentDetails.CurrentLevel,
                LastPromotion = employmentDetails.LastPromotion,
                DateOfEmployment = employmentDetails.DateOfEmployment,
                DateOfLeaving = employmentDetails.DateOfLeaving,
                YearsOfExperience = employmentDetails.YearsOfExperience,
                CUG = employmentDetails.CUG,


            };
            return View(model);
        }

        // POST: EmploymentDetails1/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EmployeeEditVm model)
        {
            var user = await _userManager.GetUserAsync(User);

            if (ModelState.IsValid)
            {
                var UserFind = await _context.EmploymentDetails.FindAsync(model.Id);


                if (UserFind == null)
                {
                    return NotFound();
                }

                UserFind.EmployeeId = model.EmployeeId;
                UserFind.Fullname = model.Fullname;
                UserFind.BloodGroup = model.BloodGroup;
                UserFind.DOB = model.DOB;
                UserFind.Gender = model.Gender;
                UserFind.MaritalStatus = model.MaritalStatus;
                UserFind.Designation = model.Designation;
                UserFind.EmployeeNo = model.EmployeeNo;
                UserFind.Email = model.Email;
                UserFind.ModeOfEmployment = model.ModeOfEmployment;
                UserFind.JobTitle = model.JobTitle;
                UserFind.EntryLevel = model.EntryLevel;
                UserFind.IsUnitHead = model.IsUnitHead;
                UserFind.IsDeptHead = model.IsDeptHead;
                UserFind.IsRegionalHead = model.IsRegionalHead;
                UserFind.CurrentLevel = model.CurrentLevel;
                UserFind.LastPromotion = model.LastPromotion;
                UserFind.DateOfEmployment = model.DateOfEmployment;
                UserFind.DateOfLeaving = model.DateOfLeaving;
                UserFind.YearsOfExperience = model.YearsOfExperience;
                UserFind.CUG = model.CUG;

                string uniqueFileName = null;

                if (model.Image != null && model.Image.Length > 0)
                {

                    string uploadFolder = Path.Combine(_hostingEnvironment.WebRootPath, "images/Employees");
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Image.FileName;
                    string filePath = Path.Combine(uploadFolder, uniqueFileName);
                    model.Image.CopyTo(new FileStream(filePath, FileMode.Create));
                    UserFind.Image = uniqueFileName;
                }

                _context.Update(UserFind);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details","EmploymentDetails1");

            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edits(EmployeeEditVm model)
        {
            var user = await _userManager.GetUserAsync(User);

            if (ModelState.IsValid)
            {
                var UserFind = await _context.EmploymentDetails.FindAsync(model.Id);


                if (UserFind == null)
                {
                    return NotFound();
                }

                UserFind.EmployeeId = model.EmployeeId;
                UserFind.Fullname = model.Fullname;
                UserFind.BloodGroup = model.BloodGroup;
                UserFind.DOB = model.DOB;
                UserFind.Gender = model.Gender;
                UserFind.MaritalStatus = model.MaritalStatus;
                UserFind.Designation = model.Designation;
                UserFind.EmployeeNo = model.EmployeeNo;
                UserFind.Email = model.Email;
                UserFind.ModeOfEmployment = model.ModeOfEmployment;
                UserFind.JobTitle = model.JobTitle;
                UserFind.EntryLevel = model.EntryLevel;
                UserFind.IsUnitHead = model.IsUnitHead;
                UserFind.IsDeptHead = model.IsDeptHead;
                UserFind.IsRegionalHead = model.IsRegionalHead;
                UserFind.CurrentLevel = model.CurrentLevel;
                UserFind.LastPromotion = model.LastPromotion;
                UserFind.DateOfEmployment = model.DateOfEmployment;
                UserFind.DateOfLeaving = model.DateOfLeaving;
                UserFind.YearsOfExperience = model.YearsOfExperience;
                UserFind.CUG = model.CUG;

                string uniqueFileName = null;

                if (model.Image != null && model.Image.Length > 0)
                {

                    string uploadFolder = Path.Combine(_hostingEnvironment.WebRootPath, "images/Employees");
                    uniqueFileName = Guid.NewGuid().ToString() + "_" + model.Image.FileName;
                    string filePath = Path.Combine(uploadFolder, uniqueFileName);
                    model.Image.CopyTo(new FileStream(filePath, FileMode.Create));
                    UserFind.Image = uniqueFileName;
                }

                _context.Update(UserFind);
                await _context.SaveChangesAsync();
                return RedirectToAction("index", "EmploymentDetails1");

            }
            return View(model);
        }

        // GET: EmploymentDetails1/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var employmentDetails = await _context.EmploymentDetails
                .FirstOrDefaultAsync(m => m.Id == id);
            if (employmentDetails == null)
            {
                return NotFound();
            }

            return View(employmentDetails);
        }

        // POST: EmploymentDetails1/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var employmentDetails = await _context.EmploymentDetails.FindAsync(id);
            _context.EmploymentDetails.Remove(employmentDetails);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task Remove(int id)
        {
            //if (id == null)
            //{
            //    return NotFound();
            //}

            var employmentDetails = await _context.EmploymentDetails.FindAsync(id);
            _context.EmploymentDetails.Remove(employmentDetails);
            await _context.SaveChangesAsync();
            //return true;
            //return RedirectToAction(nameof(Index));
        }

        private bool EmploymentDetailsExists(int id)
        {
            return _context.EmploymentDetails.Any(e => e.Id == id);
        }
    }
}
