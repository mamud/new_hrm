﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;
using NovoClients.DataAccess.Data;

namespace NovoApp.Controllers
{
    public class AppraisalsController : Controller
    {
        private readonly ApplicationDbContext _context;

        public AppraisalsController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Appraisal
        public async Task<IActionResult> Display()
        {
            var appraisal = await _context.Appraisals.ToListAsync();
            return new JsonResult(appraisal);
        }


        public IActionResult Index()
        {
            return View();
        }

        // GET: Appraisal/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appraise = await _context.Appraisals
                .FirstOrDefaultAsync(m => m.Id == id);
            if (appraise == null)
            {
                return NotFound();
            }

            return View(appraise);
        }


        public async Task<IActionResult> Detail(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appraise = await _context.Appraisals
                .FirstOrDefaultAsync(m => m.Id == id);
            if (appraise == null)
            {
                return NotFound();
            }

            return View(appraise);
        }



        // GET: Appraisal/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Appraisal/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("EmployeeId,AppraisalDate,HeadOfUnitRemark,HrRemark,Grade,AppraisalMode,EmployeeRating,ManagerRating,HrRating,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] Appraise appraise)
        {
            if (ModelState.IsValid)
            {
                _context.Add(appraise);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(appraise);
        }

        // GET: Appraisal/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appraise = await _context.Appraisals.FindAsync(id);
            if (appraise == null)
            {
                return NotFound();
            }
            return View(appraise);
        }

        // POST: Appraisal/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("EmployeeId,AppraisalDate,HeadOfUnitRemark,HrRemark,Grade,AppraisalMode,EmployeeRating,ManagerRating,HrRating,Id,AddedBy,UpdatedBy,CreateDate,UpdateDate,IsDelete")] Appraise appraise)
        {
            if (id != appraise.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(appraise);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!AppraiseExists(appraise.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(appraise);
        }

        // GET: Appraisal/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var appraise = await _context.Appraisals
                .FirstOrDefaultAsync(m => m.Id == id);
            if (appraise == null)
            {
                return NotFound();
            }

            return View(appraise);
        }

        // POST: Appraisal/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var appraise = await _context.Appraisals.FindAsync(id);
            _context.Appraisals.Remove(appraise);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        public async Task Remove(int id)
        {
            //if (id == null)
            //{
            //    return NotFound();
            //}

            var appraisal = await _context.Appraisals.FindAsync(id);
            _context.Appraisals.Remove(appraisal);
            await _context.SaveChangesAsync();
            //return true;
            //return RedirectToAction(nameof(Index));
        }

        private bool AppraiseExists(int id)
        {
            return _context.Appraisals.Any(e => e.Id == id);
        }
    }
}
