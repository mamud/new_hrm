﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Novo.Models.Entity;
using NovoClients.DataAccess.Data;
using Microsoft.AspNetCore.Identity;
using SmtpClient = MailKit.Net.Smtp.SmtpClient;
using MimeKit;
using MimeKit.Text;

namespace NovoApp.Controllers
{
    public class RequestsController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly UserManager<ApplicationUser> _userManager;

        public RequestsController(ApplicationDbContext context,
                                SignInManager<ApplicationUser> signInManager,
                                UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        // GET: Requests
        [Authorize(Roles = "User")]
        public async Task<IActionResult> Index()
        {
            var applicationDbContext = _context.Requests.Include(r => r.ApplicationUser);
            return View(await applicationDbContext.ToListAsync());
        }

        [Authorize(Roles = "DeptHead, Admin, SuperAdmin,HR,HRHead")]
        public async Task<IActionResult> RequestHI()
        {
            var user = await _userManager.GetUserAsync(User);         
            return View(await _context.Requests.Where(x => x.AssignToId == user.Id).ToListAsync());
        }

        [Authorize(Roles = "UnitHead, DeptHead, Admin, SuperAdmin")]
        public async Task<IActionResult> RequestUH()
        {
            var user = await _userManager.GetUserAsync(User);
            return View(await _context.Requests.Where(x => x.AssignToId == user.Id).ToListAsync());
        }

        // GET: Requests/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var request = await _context.Requests
                .Include(r => r.ApplicationUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (request == null)
            {
                return NotFound();
            }

            return View(request);
        }

        // GET: Requests/Create
        [Authorize(Roles = "User")]
        public IActionResult Create()
        {

            var test = (from a in _context.Users
                        join b in _context.UserRoles on a.Id equals b.UserId
                        join c in _context.Roles on b.RoleId equals c.Id
                        where a.Id == b.UserId && c.Name !="User"
                        select new
                        {
                           Value = b.UserId,
                           Text = a.FirstName + ' ' + a.LastName
                        }).ToList();
            ViewData["AssignToId"] = new SelectList(test, "Value", "Text");
            return View();
        }

        // POST: Requests/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Request request)
        {
            if (ModelState.IsValid || !ModelState.IsValid)
            {
                _context.Add(request);
                await _context.SaveChangesAsync();
                var user = await _userManager.GetUserAsync(User);
                var check = _context.Users.Where(x => x.Id == request.AssignToId).Select(s => new { s.Email }).FirstOrDefault();
                var sample = check.Email;
                var message = new MimeMessage();
                //Setting the To e-mail address
                message.To.Add(new MailboxAddress(sample));


                var tt = "akinbamidelea@novohealthafrica.org";
                message.Bcc.Add(new MailboxAddress(tt));
                //Setting the From e-mail address
                message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                //E-mail subject 
                message.Subject = "New Request";
                //E-mail message body
                message.Body = new TextPart(TextFormat.Html)
                {
                    Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                   "<br/><br/><br/><br/>" +
                "<p>Dear Colleague,</p>" +

               "<p> This is to notify that a request has been made by " + user.FirstName + " " + user.LastName +

               "<br/>on </a>" + request.CreateDate + ".</p>" +
               "Unit Head can see more information on the Task request by Clicking <a href='https://hrms.novohealthafrica.org/Requests/Details/" + request.Id + "'>Here</a>" +




                                              "<br/>Regards<br/>" +


                                               "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                };

                //Configure the e-mail
                using (var emailClient = new SmtpClient())
                {

                    emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                    emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                    emailClient.Connect("smtp.office365.com", 587, false);
                    emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2008");
                    emailClient.Send(message);
                    emailClient.Disconnect(true);
                }
                return RedirectToAction(nameof(Index));
            }
            var test = (from a in _context.Users
                        join b in _context.UserRoles on a.Id equals b.UserId
                        join c in _context.Roles on b.RoleId equals c.Id
                        where a.Id == b.UserId && c.Name != "User"
                        select new
                        {
                            Value = b.UserId,
                            Text = a.FirstName + ' ' + a.LastName
                        }).ToList();
           
            ViewData["AssignToId"] = new SelectList(test, "Value", "Text", request.AssignToId);
            return View(request);
        }

        // GET: Requests/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var request = await _context.Requests.FindAsync(id);
            if (request == null)
            {
                return NotFound();
            }
            //ViewData["AssignToId"] = new SelectList(_context.Users, "Id", "FirstName", request.AssignToId);
            return View(request);
        }

        // POST: Requests/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Request request)
        {
          

            if (ModelState.IsValid || !ModelState.IsValid)
            {
                try
                {
                    _context.Update(request);
                    await _context.SaveChangesAsync();
                    if (request.isUnitHeadApproved==true && request.isHrApproved==true)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        var check = _context.Users.Where(x => x.Id == request.EmployeeId).Select(s => new { s.Email }).FirstOrDefault();
                        var sample = check.Email;
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(sample));


                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Request";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear Colleague,</p>" +

                       "<p> This is to notify your request has been approved by Unit Head and the HR." +

                       "<br/>on </a>" + request.CreateDate + ".</p>" +
                       "Unit Head can see more information on the Task request by Clicking <a href='https://hrms.novohealthafrica.org/Requests/Details/" + request.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2008");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (request.isUnitHeadApproved==true && request.isUnitHeadDisapprove==false)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        var check = _context.Users.Where(x => x.Id == request.EmployeeId).Select(s => new { s.Email }).FirstOrDefault();
                        var sample = check.Email;
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(sample));


                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Request";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear Colleague,</p>" +

                       "<p> This is to notify your request has been approved by " + user.FirstName + " " + user.LastName +

                       "<br/>on </a>" + request.CreateDate + ".</p>" +
                       "Unit Head can see more information on the Task request by Clicking <a href='https://hrms.novohealthafrica.org/Requests/Details" + request.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2008");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (request.isHrApproved == true && request.isHrDisapprove == false)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        var check = _context.Users.Where(x => x.Id == request.EmployeeId).Select(s => new { s.Email }).FirstOrDefault();
                        var sample = check.Email;
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(sample));


                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Request";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear Colleague,</p>" +

                       "<p> This is to notify your request has been approved by " + user.FirstName + " " + user.LastName +

                       "<br/>on </a>" + request.CreateDate + ".</p>" +
                       "Unit Head can see more information on the Task request by Clicking <a href='https://hrms.novohealthafrica.org/Requests/Details/" + request.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2008");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }
                    else if (request.isHrDisapprove == true && request.isHrApproved == false)
                    {
                        var user = await _userManager.GetUserAsync(User);
                        var check = _context.Users.Where(x => x.Id == request.EmployeeId).Select(s => new { s.Email }).FirstOrDefault();
                        var sample = check.Email;
                        var message = new MimeMessage();
                        //Setting the To e-mail address
                        message.To.Add(new MailboxAddress(sample));


                        var tt = "akinbamidelea@novohealthafrica.org";
                        message.Bcc.Add(new MailboxAddress(tt));
                        //Setting the From e-mail address
                        message.From.Add(new MailboxAddress("E-mail From Novo Health Africa", "noreply@novohealthafrica.org"));
                        //E-mail subject 
                        message.Subject = "New Request";
                        //E-mail message body
                        message.Body = new TextPart(TextFormat.Html)
                        {
                            Text = "<img src='https://clients.novohealthafrica.org/img/logo.png' width='400' height='100'>" +
                           "<br/><br/><br/><br/>" +
                        "<p>Dear Colleague,</p>" +

                       "<p> This is to notify your request has been Disapproved by " + user.FirstName + " " + user.LastName +

                       "<br/>on </a>" + request.CreateDate + ".</p>" +
                       "Unit Head can see more information on the Task request by Clicking <a href='https://hrms.novohealthafrica.org/Requests/Details/" + request.Id + "'>Here</a>" +




                                                      "<br/>Regards<br/>" +


                                                       "<img src='https://clients.novohealthafrica.org/img/footer.png' width='400' height='100'>"
                        };

                        //Configure the e-mail
                        using (var emailClient = new SmtpClient())
                        {

                            emailClient.ServerCertificateValidationCallback = (sender, certificate, certChainType, errors) => true;
                            emailClient.AuthenticationMechanisms.Remove("XOAUTH2");

                            emailClient.Connect("smtp.office365.com", 587, false);
                            emailClient.Authenticate("noreply@novohealthafrica.org", "Requestpassword2008");
                            emailClient.Send(message);
                            emailClient.Disconnect(true);
                        }
                    }

                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RequestExists(request.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                if (_signInManager.IsSignedIn(User) && User.IsInRole("UnitHead"))
                {
                    return RedirectToAction("RequestUH", "Requests");
                }
                else if (_signInManager.IsSignedIn(User) && User.IsInRole("HR"))
                {
                    return RedirectToAction("RequestHI", "Requests");
                }
                return RedirectToAction(nameof(Index));
            }
            //ViewData["AssignToId"] = new SelectList(_context.Users, "Id", "FirstName", request.AssignToId);
            return View(request);
        }

        // GET: Requests/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var request = await _context.Requests
                .Include(r => r.ApplicationUser)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (request == null)
            {
                return NotFound();
            }

            return View(request);
        }

        // POST: Requests/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var request = await _context.Requests.FindAsync(id);
            _context.Requests.Remove(request);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RequestExists(int id)
        {
            return _context.Requests.Any(e => e.Id == id);
        }
    }
}
