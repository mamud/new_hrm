﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

showInPopup = (url, title) => {
    console.log("Hey")
    $.ajax({
        type: "GET",
        url: url,
        success: function (res) {
            console.log("Oga")
            console.log(res)
            $("#form-modal .modal-body").html(res);
            $("#form-modal .modal-title").html(title);
            $("#form-modal").modal('show');
        },
        error: function (err) {
            console.log(err)
        }
    })
}

showInPopupDept = (url, title) => {
    console.log("Hey")
    $.ajax({
        type: "GET",
        url: url,
        success: function (res) {
            console.log("Oga")
            console.log(res)
            $("#form-modal .modal-body").html(res);
            $("#form-modal .modal-title").html(title);
            $("#form-modal").modal('show');
        },
        error: function (err) {
            console.log(err)
        }
    })
}

showInPopupDetail = () => {
    console.log('Hey')
    var tr = $(this).closest("tr");
    var data = table.row(tr).data();
    console.log(data)
}

table.on('click', '.open-details-modal', function () {
    console.log('helloooo')
    var tr = $(this).closest("tr");
    var data = table.row(tr).data();

    var tmpl = $.templates("#details-modal-template");
    var html = tmpl.render(data);
    $("#details-modal-body").html(html);
    $('#details-modal').modal('show')
})


jQueryAjaxPost = form => {
    try {
        $.ajax({
            type: 'POST',
            url: form.action,
            data: new FormData(form),
            contentType: false,
            processData: false,
            success: function (res) {
                if (res.isValid) {
                    $("#form-modal .modal-body").html(res.html);
                    $("#form-modal .modal-body").html('');
                    $("#form-modal .modal-title").html('');
                    $("#form-modal").modal('hide');
                }
                else
                    $("#form-modal .modal-body").html(res.html);
            },
            error: function (err) {
                console.log('there\'s an error');
                console.log(form.action)
                console.log(err)
            }
        })
    } catch (e) {
        console.log(e);
    }

    return false;
}